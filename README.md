Scheduler
=========

The Scheduler application allows the user to view, edit, create, and delete
customer and appointment data, as well as view select reports on the data.

Usage directions:  After entering your login information, a pop-up window will
display showing either your appointments that begin in the next 15 minutes or a
message indicating you have no upcoming appointments.  The main screen
initially displays a table of all customers.  You can use the tabs at the top
to switch between the customers, appointments, and reports views.  On both the
customers and appointments views, press the buttons at the bottom to add a new
customer or appointment, or to edit or delete the selected customer or
appointment.  The 'add' and 'update' buttons will open new windows with forms
for entering and editing the customer or appointment information.  The
appointments view also has radio buttons for filtering the table.  'All'
displays all appointments (past or future).  The 'This month' and 'This week'
buttons filter the list to display only appointments from the current month or
week (Monday through Sunday) whether they have occurred yet or not.  In the
reports view, clicking on one of the bars will display the given report.  All
changes to the customer and appointment data are automatically saved to the
database.

Built with:
-----------

* IDE:  IntelliJ IDEA 2020.3.3 (Community Edition)
* JDK:  OpenJDK 11.0.11
* JavaFX:  OpenJFX 11.0.11
* MySQL Connector driver:  mysql-connector-java-8.0.23

Note:
-----

The database setup for the current version will not work.  This was a school
project, and they set up development databases for use in the class.  This
database no longer exists.  If I have time I will probably convert this to use
an embedded database so the program can be fully explored when run.
