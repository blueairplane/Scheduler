package scheduler;

import javafx.application.Application;
import javafx.stage.Stage;
import scheduler.controller.ViewLoader;

import java.util.ResourceBundle;

/**
 * Main entry point for the Scheduler application.
 *
 * Main features:
 * <ul>
 *     <li>View, create, edit, and delete customer data</li>
 *     <li>View, create, edit, and delete appointments</li>
 *     <li>View reports of the customer and appointment data</li>
 * </ul>
 */
public class Main extends Application {

    /**
     * Entry point into the application.  Loads the internationalization resources and opens the
     * login window.
     *
     * @param primaryStage the primary stage for this application
     * @throws Exception if there is a problem loading resources or views
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        var resources = ResourceBundle.getBundle("scheduler.Scheduler");
        String windowTitle = resources.getString("windowTitle");
        ViewLoader.setResources(resources);
        ViewLoader.setWindowTitle(windowTitle);
        ViewLoader.show("Login", primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
