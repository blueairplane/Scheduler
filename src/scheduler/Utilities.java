package scheduler;

import javafx.scene.control.TextFormatter;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

/**
 * Static utility functions for the Scheduler.
 */
public class Utilities {
    /**
     * The string is entirely comprised of digits.
     */
    private static final Pattern allNumbers = Pattern.compile("\\d*");
    /**
     * Time zone of the business office.
     */
    private static final ZoneId businessZone = ZoneId.of("EST", ZoneId.SHORT_IDS);
    /**
     * Local opening time of the business office.
     */
    private static final LocalTime businessStartTime = LocalTime.of(8, 0);
    /**
     * Local closing time of the business office.
     */
    private static final LocalTime businessEndTime = LocalTime.of(22, 0);

    /**
     * Returns a formatter for JavaFX text input that limits the number of characters to {@code
     * maxLength}.  If text is pasted to the end of the current text, any overflow is truncated.
     * Any other pasting of text too long to fit will result in a noop.
     *
     * The lambda here is shorter and more directly shows the intention than the alternative of
     * an anonymous class.
     *
     * @param maxLength the maximum number of characters allowed in the text input
     * @return a TextFormatter for a JavaFX text input
     */
    public static TextFormatter<?> maxLengthFormatter(int maxLength) {
        return new TextFormatter<>(change -> {
            var oldLength = change.getControlText().length();
            var newText = change.getControlNewText();
            if (change.getRangeEnd() != oldLength && newText.length() > maxLength) return null;
            else {
                if (newText.length() > maxLength) newText = newText.substring(0, maxLength);
                change.setRange(0, oldLength);
                change.setText(newText);
                return change;
            }
        });
    }

    /**
     * Returns a formatter for JavaFX text input that limits the input to a number of no more than
     * {@code maxDigits} digits.
     *
     * The lambda here is shorter and more directly shows the intention than the alternative of
     * an anonymous class.
     *
     * @param maxDigits the maximum number of digits allowed
     * @return a TextFormatter for a JavaFX text input
     */
    public static TextFormatter<?> numberFormatter(int maxDigits) {
        return new TextFormatter<>(change -> {
            var newText = change.getControlNewText();
            return newText.length() <= maxDigits
                    && allNumbers.matcher(newText).matches()
                    ? change : null;
        });
    }

    /**
     * Converts an {@code Instant} object to a string usable in SQL.  There is no time zone
     * conversion, so this assumes the conversion should be to the UTC time zone.
     *
     * @param instant the {@code Instant} object to be converted
     * @return a datetime string in the format "yyyy-mm-ddThh:mm:ss"
     */
    public static String instantToSqlDateTime(Instant instant) {
        return instant.toString().replace("Z", "");
    }

    /**
     * Converts a datetime string retrieved from a SQL query to an {@code Instant}.  A SQL
     * datetime is time zone-independent, and this method assumes the given datetime is in the UTC
     * time zone.
     *
     * @param dateTime the string representation of a SQL datetime
     * @return an {@code Instant} of the datetime string in the UTC timezone
     */
    public static Instant sqlDateTimeToInstant(String dateTime) {
        var format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(dateTime, format).atZone(ZoneId.of("UTC")).toInstant();
    }

    /**
     * Determines if an appointment's start and end times are valid and fall within a single
     * business day.
     *
     * @param start the start of the appointment
     * @param end the end of the appointment
     * @return true if the start is before the end and both times occur in the same business day,
     * false otherwise
     */
    public static boolean validTimes(Instant start, Instant end) {
        if (!start.isBefore(end)) return false;

        var zonedStart = start.atZone(businessZone);
        var zonedEnd = end.atZone(businessZone);
        /*
         with times standardized to the office time zone, they must be on the same date for the
         appointment to fit within a single business day
        */
        if (zonedStart.getDayOfMonth() != zonedEnd.getDayOfMonth()) return false;

        var date = zonedStart.toLocalDate();
        var businessStart = ZonedDateTime.of(date, businessStartTime, businessZone);
        var businessEnd = ZonedDateTime.of(date, businessEndTime, businessZone);
        /*
         the start of the business day must not occur after the appointment's start or end
         time, and the end of the business day must not occur before the appointment's start or
         end time
        */
        return !businessStart.isAfter(zonedStart) && !businessEnd.isBefore(zonedStart)
                && !businessStart.isAfter(zonedEnd) && !businessEnd.isBefore(zonedEnd);
    }
}
