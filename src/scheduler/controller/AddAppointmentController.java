package scheduler.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import scheduler.dao.AppointmentDao;
import scheduler.dao.ContactDao;
import scheduler.dao.CustomerDao;
import scheduler.dao.UserDao;
import scheduler.model.Appointment;
import scheduler.model.Contact;

import java.sql.SQLException;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;

import static scheduler.Utilities.*;
import static scheduler.controller.ViewLoader.getAuthorizedUser;

/**
 * Controls the 'Add Appointment' form and its enclosing window.
 */
public class AddAppointmentController extends Controller {
    /**
     * Root node of this view.
     */
    @FXML private VBox addAppointmentRoot;
    /**
     * Text entry for the new appointment's title.
     */
    @FXML private TextField addAppointmentTitle;
    /**
     * Text entry for the new appointment's description.
     */
    @FXML private TextField addAppointmentDescription;
    /**
     * Text entry for the new appointment's type.
     */
    @FXML private TextField addAppointmentType;
    /**
     * Text entry for the new appointment's location.
     */
    @FXML private TextField addAppointmentLocation;
    /**
     * Date selector for the new appointment's start date.
     */
    @FXML private DatePicker addAppointmentStartDate;
    /**
     * Pull-down selector of times for the new appointment's start time.
     */
    @FXML private ComboBox<LocalTime> addAppointmentStartTime;
    /**
     * Date selector for the new appointment's end date.
     */
    @FXML private DatePicker addAppointmentEndDate;
    /**
     * Pull-down selector of times for the new appointment's end time.
     */
    @FXML private ComboBox<LocalTime> addAppointmentEndTime;
    /**
     * Pull-down selector of contacts.
     */
    @FXML private ComboBox<Contact> addAppointmentContact;
    /**
     * Text entry for the ID number of the customer associated with the new appointment.
     */
    @FXML private TextField addAppointmentCustomerId;
    /**
     * Text entry for the ID number of the user associated with the new appointment.
     */
    @FXML private TextField addAppointmentUserId;
    /**
     * Button for submitting appointment information.
     */
    @FXML private Button addAppointmentButton;
    /**
     * Error message for problems with adding the new appointment.
     */
    @FXML private Label addAppointmentErrorAdd;
    /**
     * Error message for database errors.
     */
    @FXML private Label addAppointmentErrorDb;
    /**
     * Error message for invalid customer ID.
     */
    @FXML private Label addAppointmentErrorInvalidCustomerId;
    /**
     * Error message for invalid user ID.
     */
    @FXML private Label addAppointmentErrorInvalidUserId;
    /**
     * Error message for invalid appointment times.
     */
    @FXML private Label addAppointmentErrorInvalidTimes;
    /**
     * Error message for overlapping appointment times.
     */
    @FXML private Label addAppointmentErrorOverlap;

    /**
     * Sets up the form.  Installs formatters that enforce the length restrictions of the text
     * columns from the database.
     *
     * The lambda used here takes several fewer lines and is much more clear and straightforward
     * than the alternative of an anonymous overridden class as the parameter to the {@code
     * addListener} method.  The intention is "when the focused property changes, perform this
     * action."  The mechanism behind this is laid bare with the use of an overridden class (when
     * the property's value becomes invalid -- that is, it has changed and must be reevaluated --
     * the {@code invalidated()} method is called), and that only clutters the code and muddies
     * the simple intent of performing an action when the button looses focus.
     *
     * @throws SQLException if there is a database error
     */
    @FXML
    public void initialize() throws SQLException {
        var contactList = ContactDao.withUser(getAuthorizedUser()).getAll();
        var contacts = FXCollections.observableArrayList(contactList);
        addAppointmentContact.setItems(contacts);
        var timeList = new ArrayList<LocalTime>();
        for (int hour = 0; hour < 24; hour++) {
            timeList.add(LocalTime.of(hour, 0));
        }
        var times = FXCollections.observableArrayList(timeList);
        addAppointmentStartTime.setItems(times);
        addAppointmentEndTime.setItems(times);

        addAppointmentTitle.setTextFormatter(maxLengthFormatter(50));
        addAppointmentDescription.setTextFormatter(maxLengthFormatter(50));
        addAppointmentType.setTextFormatter(maxLengthFormatter(50));
        addAppointmentLocation.setTextFormatter(maxLengthFormatter(50));
        addAppointmentCustomerId.setTextFormatter(numberFormatter(10));
        addAppointmentUserId.setTextFormatter(numberFormatter(10));
        addAppointmentButton.focusedProperty().addListener(changed -> clearErrorMessages());
    }

    /**
     * Attempts to add a new appointment to the database with the information entered into the form.
     * If any fields are empty or there is a database error, it will display an error message and
     * abort the add operation.  If the new appointment is successfully added, this window will close.
     */
    @FXML
    private void addAppointment() throws SQLException {
        clearErrorMessages();
        var title = addAppointmentTitle.getText();
        var description = addAppointmentDescription.getText();
        var location = addAppointmentLocation.getText();
        var type = addAppointmentType.getText();
        var startDate = addAppointmentStartDate.getValue();
        var startTime = addAppointmentStartTime.getValue();
        var endDate = addAppointmentEndDate.getValue();
        var endTime = addAppointmentEndTime.getValue();
        var customerId = addAppointmentCustomerId.getText();
        var userId = addAppointmentUserId.getText();
        var contact = addAppointmentContact.getValue();
        if (title.equals("") || description.equals("") || location.equals("") || type.equals("")
                || startDate == null || startTime == null || endDate == null || endTime == null
                || customerId.equals("") || userId.equals("") || contact == null) {
            System.err.println("Could not store appointment: not all fields filled");
            addAppointmentErrorAdd.setVisible(true);
            return;
        }

        var start = ZonedDateTime.of(startDate, startTime, ZoneId.systemDefault()).toInstant();
        var end = ZonedDateTime.of(endDate, endTime, ZoneId.systemDefault()).toInstant();
        if (!validTimes(start, end)) {
            System.err.println("Could not store appointment: invalid times");
            addAppointmentErrorInvalidTimes.setVisible(true);
            return;
        }

        var customer = CustomerDao.withUser(getAuthorizedUser()).get(Integer.parseInt(customerId));
        if (customer == null) {
            System.err.println("Invalid customer ID");
            addAppointmentErrorInvalidCustomerId.setVisible(true);
            return;
        }
        var user = UserDao.withUser(getAuthorizedUser()).get(Integer.parseInt(userId));
        if (user == null) {
            System.err.println("Invalid user ID");
            addAppointmentErrorInvalidUserId.setVisible(true);
            return;
        }

        var customerAppointments =
                AppointmentDao.withUser(getAuthorizedUser()).getAllForCustomer(customer);
        var newAppointment = new Appointment(title, description, location, type, start, end,
                customer, user, contact);
        if (newAppointment.conflictsWithAny(customerAppointments)) {
            System.err.println("Could not store appointment: overlaps with another of " +
                    customer.getName() + "'s appointments");
            addAppointmentErrorOverlap.setVisible(true);
            return;
        }

        try {
            AppointmentDao.withUser(getAuthorizedUser()).add(newAppointment);
        } catch (SQLException e) {
            System.err.println("Database error: " + e.getMessage());
            addAppointmentErrorDb.setVisible(true);
            return;
        }
        closeStage();
    }

    /**
     * Clears the entry fields and closes this window,
     */
    @FXML
    private void cancel() {
        closeStage();
    }

    /**
     * Clean up and closes this window.
     */
    private void closeStage() {
        addAppointmentTitle.requestFocus();
        addAppointmentTitle.clear();
        addAppointmentDescription.clear();
        addAppointmentType.clear();
        addAppointmentLocation.clear();
        addAppointmentContact.setItems(null);
        addAppointmentCustomerId.clear();
        addAppointmentUserId.clear();
        addAppointmentStartDate.setValue(null);
        addAppointmentEndDate.setValue(null);
        addAppointmentStartTime.setValue(null);
        addAppointmentEndTime.setValue(null);
        clearErrorMessages();
        addAppointmentRoot.getScene().getWindow().hide();
    }

    /**
     * Hides all error messages.
     */
    private void clearErrorMessages() {
        addAppointmentErrorAdd.setVisible(false);
        addAppointmentErrorDb.setVisible(false);
        addAppointmentErrorInvalidTimes.setVisible(false);
        addAppointmentErrorOverlap.setVisible(false);
        addAppointmentErrorInvalidCustomerId.setVisible(false);
        addAppointmentErrorInvalidUserId.setVisible(false);
    }
}
