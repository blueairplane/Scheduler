package scheduler.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import scheduler.dao.CountryDao;
import scheduler.dao.CustomerDao;
import scheduler.dao.DivisionDao;
import scheduler.model.Country;
import scheduler.model.Customer;
import scheduler.model.Division;

import java.sql.SQLException;
import java.util.List;

import static scheduler.Utilities.maxLengthFormatter;
import static scheduler.controller.ViewLoader.getAuthorizedUser;

/**
 * Controls the 'Add Customer' form and its enclosing window.
 */
public class AddCustomerController extends Controller {
    /**
     * Root node for this view.
     */
    @FXML public VBox addCustomerRoot;
    /**
     * Text entry for the new customer's full name.
     */
    @FXML private TextField addCustomerName;
    /**
     * Text entry for the new customer's phone number.  It is not checked for formatting.
     */
    @FXML private TextField addCustomerPhone;
    /**
     * Text entry for the new customer's address, up to the first-level-division.  Examples:
     * <ul>
     *     <li>U.S. address: 123 ABC Street, White Plains</li>
     *     <li>Canadian address: 123 ABC Street, Newmarket</li>
     *     <li>UK address: 123 ABC Street, Greenwich, London</li>
     * </ul>
     */
    @FXML private TextField addCustomerAddress;
    /**
     * Text entry for the new customer's postal code.  It is not check for validity.
     */
    @FXML private TextField addCustomerPostalCode;
    /**
     * Pull-down selector of first-level divisions.
     */
    @FXML private ComboBox<Division> addCustomerDivision;
    /**
     * Pull-down selector of countries.
     */
    @FXML private ComboBox<Country> addCustomerCountry;
    /**
     * Button for submitting customer information.
     */
    @FXML private Button addCustomerButton;
    /**
     * Error message for problems with adding the new customer.
     */
    @FXML private Label addCustomerErrorAdd;
    /**
     * Error message for database errors.
     */
    @FXML private Label addCustomerErrorDb;

    /**
     * Sets up the form.  Installs formatters that enforce the length restrictions of the text
     * columns from the database.
     *
     * The lambda used here takes several fewer lines and is much more clear and straightforward
     * than the alternative of an anonymous overridden class as the parameter to the {@code
     * addListener} method.  The intention is "when the focused property changes, perform this
     * action."  The mechanism behind this is laid bare with the use of an overridden class (when
     * the property's value becomes invalid -- that is, it has changed and must be reevaluated --
     * the {@code invalidated()} method is called), and that only clutters the code and muddies
     * the simple intent of performing an action when the button looses focus.
     *
     * @throws SQLException if there is a database error
     */
    @FXML
    public void initialize() throws SQLException {
        var countryList = CountryDao.withUser(getAuthorizedUser()).getAll();
        var countries = FXCollections.observableArrayList(countryList);
        addCustomerCountry.setItems(countries);
        addCustomerName.setTextFormatter(maxLengthFormatter(50));
        addCustomerPhone.setTextFormatter(maxLengthFormatter(50));
        addCustomerAddress.setTextFormatter(maxLengthFormatter(100));
        addCustomerPostalCode.setTextFormatter(maxLengthFormatter(50));
        addCustomerButton.focusedProperty().addListener(changed -> clearErrorMessages());
    }

    /**
     * Attempts to add a new customer to the database with the information entered into the form.
     * If any fields are empty or there is a database error, it will display an error message and
     * abort the add operation.  If the new customer is successfully added, this window will close.
     */
    @FXML
    private void addCustomer() {
        var name = addCustomerName.getText();
        var address = addCustomerAddress.getText();
        var postalCode = addCustomerPostalCode.getText();
        var division = addCustomerDivision.getValue();
        var phone = addCustomerPhone.getText();
        if (name.equals("") || address.equals("") || postalCode.equals("") || phone.equals("")
                || division == null) {
            System.err.println("Could not store customer: not all fields filled");
            clearErrorMessages();
            addCustomerErrorAdd.setVisible(true);
            return;
        }
        var newCustomer = new Customer(name, address, postalCode, division, phone);
        try {
            CustomerDao.withUser(getAuthorizedUser()).add(newCustomer);
        } catch (SQLException e) {
            clearErrorMessages();
            addCustomerErrorDb.setVisible(true);
            System.err.println("Database error: " + e.getMessage());
            return;
        }
        closeStage();
    }

    /**
     * Clears the entry fields and closes this window,
     */
    @FXML
    private void cancel() {
        closeStage();
    }

    /**
     * Clean up and closes this window.
     */
    private void closeStage() {
        addCustomerName.requestFocus();
        addCustomerName.clear();
        addCustomerPhone.clear();
        addCustomerAddress.clear();
        addCustomerPostalCode.clear();
        addCustomerCountry.setValue(null);
        addCustomerDivision.setItems(null);
        addCustomerDivision.setDisable(true);
        clearErrorMessages();
        addCustomerRoot.getScene().getWindow().hide();
    }

    /**
     * Hides all error messages.
     */
    private void clearErrorMessages() {
        addCustomerErrorAdd.setVisible(false);
        addCustomerErrorDb.setVisible(false);
    }

    /**
     * Attempts to retrieve the country's list of divisions and populate and activate the
     * division combo box when the user selects a country from the country combo box.
     */
    @FXML
    private void countryChanged() {
        var country = addCustomerCountry.getValue();
        if (country == null) {
            return;
        }
        var divisionDao = DivisionDao.withUser(getAuthorizedUser());
        List<Division> divisionList;
        try {
            divisionList = divisionDao.getAllFromCountry(country);
        } catch (SQLException e) {
            clearErrorMessages();
            addCustomerErrorDb.setVisible(true);
            addCustomerCountry.setValue(null);
            addCustomerDivision.setItems(null);
            addCustomerDivision.setDisable(true);
            System.err.println("Database error: " + e.getMessage());
            return;
        }
        var divisions = FXCollections.observableArrayList(divisionList);
        addCustomerDivision.setItems(divisions);
        addCustomerDivision.setDisable(false);
    }
}
