package scheduler.controller;

import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import scheduler.dao.AppointmentDao;
import scheduler.model.Appointment;

import java.io.IOException;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static scheduler.controller.ViewLoader.getAuthorizedUser;

/**
 * Controls the appointments table and its related buttons.
 */
public class AppointmentsController extends Controller {
    /**
     * Indicates data is being loaded.
     */
    @FXML private ProgressIndicator progressIndicator;
    /**
     * Table showing customer information pulled from the database.
     */
    @FXML private TableView<Appointment> appointmentsTable;
    /**
     * Appointment ID column of the table.
     */
    @FXML private TableColumn<Appointment, Integer> appointmentsIdColumn;
    /**
     * Appointment title column of the table.
     */
    @FXML private TableColumn<Appointment, String> appointmentsTitleColumn;
    /**
     * Appointment description column of the table.
     */
    @FXML private TableColumn<Appointment, String> appointmentsDescriptionColumn;
    /**
     * Appointment location column of the table.
     */
    @FXML private TableColumn<Appointment, String> appointmentsLocationColumn;
    /**
     * Appointment contact name column of the table.
     */
    @FXML private TableColumn<Appointment, String> appointmentsContactColumn;
    /**
     * Appointment type column of the table.
     */
    @FXML private TableColumn<Appointment, String> appointmentsTypeColumn;
    /**
     * Appointment start time column of the table.  It is in 24-hour local time (based on the
     * user's computer settings) and in the following format: YYYY/MM/DD HH:MM
     */
    @FXML private TableColumn<Appointment, String> appointmentsStartTimeColumn;
    /**
     * Appointment end time column of the table.  It is in 24-hour local time (based on the
     * user's computer settings) and in the following format: YYYY/MM/DD HH:MM
     */
    @FXML private TableColumn<Appointment, String> appointmentsEndTimeColumn;
    /**
     * Appointment customer ID column of the table.
     */
    @FXML private TableColumn<Appointment, Number> appointmentsCustomerIdColumn;
    /**
     * Error message for database access problems.
     */
    @FXML private Label appointmentsErrorDb;
    /**
     * Error message for when an appointment cannot be deleted.
     */
    @FXML private Label appointmentsErrorDelete;
    /**
     * 'Appointment deleted' message.
     */
    @FXML private Label appointmentsMessageDelete;
    /**
     * List of appointments with which to populate the table.
     */
    private final ObservableList<Appointment> appointments = FXCollections.observableArrayList();
    /**
     * Filtered version of the appointment list for display.
     */
    private final FilteredList<Appointment> filteredAppointments = new FilteredList<>(appointments);

    /**
     * Sets up the table.
     *
     * Lambdas used:
     * <dl>
     *     <dt>appointmentsTable.getColumns().forEach(...)</dt>
     *         <dd>This lambda is much more succinct than an anonymous class, and the {@code
     *         forEach()} conveys the intent of the line more directly than a for loop.</dd>
     *     <dt>appointmentsContactColumn.setCellValueFactory(...)</dt>
     *     <dt>appointmentsCustomerIdColumn.setCellValueFactory(...)</dt>
     *         <dd>The simple {@code PropertyValueFactory} cannot be used in these cases,
     *         because the values needed for these columns are not simple attributes of the
     *         {@code Appointment} class.  While methods could be added to that class to enable
     *         the use of {@code PropertyValueFactory}, altering another class for single-use
     *         convenience here is not a good design.  The lambdas here allow us to extract the
     *         value needed without needlessly complicating the data class.</dd>
     *     <dt>appointmentsStartTimeColumn.setCellValueFactory(...)</dt>
     *     <dt>appointmentsEndTimeColumn.setCellValueFactory(...)</dt>
     *         <dd>Similar to the above cases, the attributes needed for these columns cannot be
     *         used as-is.  In these cases, the time values must be converted to the local time
     *         zone and formatted for display.  Without lambdas, this would have to be done
     *         either in a separate method or in an anonymous class that is passed to the {@code
     *         setCellValueFactory} method.  We do not need the extra complexity of an anonymous
     *         class for the simple formatting being done, and a separate method (for each)
     *         needlessly clutters this class with one-use methods.</dd>
     * </dl>
     */
    @FXML
    public void initialize() {
        appointmentsTable.getColumns().forEach(column -> column.setReorderable(false));
        appointmentsIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        appointmentsTitleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        appointmentsDescriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        appointmentsLocationColumn.setCellValueFactory(new PropertyValueFactory<>("location"));
        appointmentsContactColumn.setCellValueFactory(appointment ->
                new ReadOnlyStringWrapper(appointment.getValue().getContact().getName()));
        appointmentsTypeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        appointmentsStartTimeColumn.setCellValueFactory(appointment -> {
            var zoneId = ZoneId.systemDefault();
            var dateTime = appointment.getValue().getStart().atZone(zoneId);
            return new ReadOnlyStringWrapper(dateTime.toLocalDate() + " " + dateTime.toLocalTime());
        });
        appointmentsEndTimeColumn.setCellValueFactory(appointment -> {
            var zoneId = ZoneId.systemDefault();
            var dateTime = appointment.getValue().getEnd().atZone(zoneId);
            return new ReadOnlyStringWrapper(dateTime.toLocalDate() + " " + dateTime.toLocalTime());
        });
        appointmentsCustomerIdColumn.setCellValueFactory(appointment ->
                new ReadOnlyIntegerWrapper(appointment.getValue().getCustomer().getId()));
        appointmentsTable.setItems(filteredAppointments);
    }

    /**
     * Loads appointment data from the database to keep the table correctly populated.
     */
    public void update() {
        clearMessages();
        progressIndicator.setVisible(true);
        var thread = new Thread(new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    var appointmentDao = AppointmentDao.withUser(getAuthorizedUser());
                    appointments.setAll(appointmentDao.getAll());
                } catch (SQLException e) {
                    System.err.println("Database error while loading appointment data: " + e.getMessage());
                    appointmentsErrorDb.setVisible(true);
                    appointments.clear();
                } finally {
                    progressIndicator.setVisible(false);
                }
                return null;
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Hides all messages.
     */
    private void clearMessages() {
        appointmentsErrorDelete.setVisible(false);
        appointmentsMessageDelete.setVisible(false);
    }

    /**
     * Removes filter from table, showing all appointments.
     *
     * This lambda is an extremely simple {@code Predicate} that essentially removes the filter
     * from the list by allowing all items through.  Another other solution would use several
     * more lines of code and add no clarity.
     */
    @FXML
    private void viewAll() {
        filteredAppointments.setPredicate(appointment -> true);
    }

    /**
     * Filters the table to show only appointments occurring in the current month.
     *
     * This lambda is a {@code Predicate} that filters out appointments that do fall in the
     * current month and year.  It more simply and directly shows the intention than other
     * solutions like an anonymous class.
     */
    @FXML
    private void viewMonth() {
        filteredAppointments.setPredicate(appointment -> {
            var appointmentStart = appointment.getStart().atZone(ZoneId.systemDefault());
            var today = ZonedDateTime.now();
            return appointmentStart.getYear() == today.getYear()
                    && appointmentStart.getMonth() == today.getMonth();
        });
    }

    /**
     * Filters the table to show only appointments occurring in the current week (Monday - Sunday).
     *
     * This lambda is a {@code Predicate} that filters out appointments that do fall in the
     * current week.  It more simply and directly shows the intention than other solutions like
     * an anonymous class.
     */
    @FXML
    private void viewWeek() {
        filteredAppointments.setPredicate(appointment -> {
            var appointmentDate = appointment.getStart().atZone(ZoneId.systemDefault()).toLocalDate();
            var today = LocalDate.now();
            var weekBegin = today.with(DayOfWeek.MONDAY);
            var weekEnd = today.with(DayOfWeek.SUNDAY);
            return !appointmentDate.isBefore(weekBegin) && !appointmentDate.isAfter(weekEnd);
        });
    }

    /**
     * Opens a window with a form for adding a new appointment.  This window will reopen when the
     * new window closes.
     */
    @FXML
    private void addAppointment(ActionEvent event) throws IOException {
        clearMessages();
        var thisStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        thisStage.hide();
        ViewLoader.showAndWait("AddAppointment");
        thisStage.show();
        update();
    }

    /**
     * Opens a window with a form for updating the selected appointment's information.
     */
    @FXML
    private void updateAppointment(ActionEvent event) throws IOException {
        clearMessages();
        var appointment = appointmentsTable.getSelectionModel().getSelectedItem();
        if (appointment == null) return;
        var thisStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        thisStage.hide();
        ViewLoader.showAndWait("UpdateAppointment", appointment);
        thisStage.show();
        update();
    }

    /**
     * Removes the selected appointment from the database.
     */
    @FXML
    private void deleteAppointment () {
        clearMessages();
        var appointment = appointmentsTable.getSelectionModel().getSelectedItem();
        if (appointment == null) return;
        try {
            if (AppointmentDao.withUser(getAuthorizedUser()).delete(appointment)) {
                appointments.remove(appointment);
                var appointmentDeletedMessage =
                        ViewLoader.getResource("Appointment.message.delete")
                                + appointment.getId() + " - " + appointment.getType();
                appointmentsMessageDelete.setText(appointmentDeletedMessage);
                appointmentsMessageDelete.setVisible(true);
            }
        } catch (SQLException e) {
            appointmentsErrorDelete.setVisible(true);
            System.err.println("Database error while deleting appointment: " + e.getMessage());
        }
    }
}
