package scheduler.controller;

/**
 * Base class for controllers that can receive data.
 */
public abstract class Controller {
    /**
     * Receives data for this controller to use.
     *
     * @param data the data.  It should be cast to be used properly.
     */
    public void setData(Object data) { }
}
