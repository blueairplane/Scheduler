package scheduler.controller;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import scheduler.dao.CustomerDao;
import scheduler.model.Customer;

import java.io.IOException;
import java.sql.SQLException;

import static scheduler.controller.ViewLoader.getAuthorizedUser;

/**
 * Controls the customer table and its related buttons.
 */
public class CustomersController extends Controller {
    /**
     * Indicates data is being loaded.
     */
    @FXML private ProgressIndicator progressIndicator;
    /**
     * Table showing customer information pulled from the database.
     */
    @FXML private TableView<Customer> customersTable;
    /**
     * Customer ID column of the table.
     */
    @FXML private TableColumn<Customer, Integer> customersIdColumn;
    /**
     * Customer name column of the table.
     */
    @FXML private TableColumn<Customer, String> customersNameColumn;
    /**
     * Address column of the table.
     */
    @FXML private TableColumn<Customer, String> customersAddressColumn;
    /**
     * Postal code column of the table.
     */
    @FXML private TableColumn<Customer, String> customersPostalCodeColumn;
    /**
     * First-level division column of the table.
     */
    @FXML private TableColumn<Customer, String> customersDivisionColumn;
    /**
     * Country column of the table.
     */
    @FXML private TableColumn<Customer, String> customersCountryColumn;
    /**
     * Phone number column of the table.
     */
    @FXML private TableColumn<Customer, String> customersPhoneColumn;
    /**
     * Error message for database access problems.
     */
    @FXML private Label customersErrorDb;
    /**
     * Error message for when a customer cannot be deleted.
     */
    @FXML private Label customersErrorDelete;
    /**
     * 'Customer deleted' message.
     */
    @FXML private Label customersMessageDelete;
    /**
     * List of customers with which to populate the table.
     */
    private final ObservableList<Customer> customers = FXCollections.observableArrayList();

    /**
     * Sets up the table.
     * 
     * Lambdas used:
     * <dl>
     *     <dt>customersTable.getColumns().forEach(...)</dt>
     *         <dd>This lambda is much more succinct than an anonymous class, and the {@code
     *         forEach()} conveys the intent of the line more directly than a for loop.</dd>
     *     <dt>customersDivisionColumn.setCellValueFactory(...)</dt>
     *     <dt>customersCountryColumn.setCellValueFactory(...)</dt>
     *         <dd>The simple {@code PropertyValueFactory} cannot be used in these cases,
     *         because the values needed for these columns are not simple attributes of the
     *         {@code Customer} class.  While methods could be added to that class to enable the
     *         use of {@code PropertyValueFactory}, altering another class for single-use
     *         convenience here is not a good design.  The lambdas here allow us to extract the
     *         value needed without needlessly complicating the data class.</dd>
     * </dl>
     */
    @FXML
    public void initialize() {
        customersTable.getColumns().forEach(column -> column.setReorderable(false));
        customersIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        customersNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        customersAddressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        customersPostalCodeColumn.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        customersDivisionColumn.setCellValueFactory(customer ->
                new ReadOnlyStringWrapper(customer.getValue().getDivision().getName()));
        customersCountryColumn.setCellValueFactory(customer ->
                new ReadOnlyStringWrapper(customer.getValue().getCountry().getName()));
        customersPhoneColumn.setCellValueFactory(new PropertyValueFactory<>("phone"));
        customersTable.setItems(customers);
    }

    /**
     * Loads customer data from the database to keep the table correctly populated.
     */
    public void update() {
        clearMessages();
        progressIndicator.setVisible(true);
        var thread = new Thread(new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    var customerDao = CustomerDao.withUser(getAuthorizedUser());
                    customers.setAll(customerDao.getAll());
                } catch (SQLException e) {
                    System.err.println("Database error while loading customer data: " + e.getMessage());
                    customersErrorDb.setVisible(true);
                    customers.clear();
                } finally {
                    progressIndicator.setVisible(false);
                }
                return null;
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Hides all messages.
     */
    private void clearMessages() {
        customersErrorDelete.setVisible(false);
        customersMessageDelete.setVisible(false);
    }

    /**
     * Opens a window with a form for adding a new customer.  This window will reopen when the
     * new window closes.
     */
    @FXML
    private void addCustomer(ActionEvent event) throws IOException {
        clearMessages();
        var thisStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        thisStage.hide();
        ViewLoader.showAndWait("AddCustomer");
        thisStage.show();
        update();
    }

    /**
     * Opens a window with a form for updating the selected customer's information.
     */
    @FXML
    private void updateCustomer(ActionEvent event) throws IOException {
        clearMessages();
        var customer = customersTable.getSelectionModel().getSelectedItem();
        if (customer == null) return;
        var thisStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        thisStage.hide();
        ViewLoader.showAndWait("UpdateCustomer", customer);
        thisStage.show();
        update();
    }

    /**
     * Removes the selected customer from the database, along with their appointments.
     */
    @FXML
    private void deleteCustomer () {
        clearMessages();
        var customer = customersTable.getSelectionModel().getSelectedItem();
        if (customer == null) return;
        try {
            if (CustomerDao.withUser(getAuthorizedUser()).delete(customer)) {
                customers.remove(customer);
                var customerDeletedMessage =
                        ViewLoader.getResource("Customer.message.delete")
                                + customer.getId() + " - " + customer.getName();
                customersMessageDelete.setText(customerDeletedMessage);
                customersMessageDelete.setVisible(true);
            }
        } catch (SQLException e) {
            customersErrorDelete.setVisible(true);
            System.err.println("Database error while deleting customer: " + e.getMessage());
        }
    }
}
