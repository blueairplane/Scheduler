package scheduler.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.Region;
import scheduler.dao.AppointmentDao;
import scheduler.dao.UserValidator;
import scheduler.model.Appointment;
import scheduler.model.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 * Controls the login form and its enclosing window.  Allows for access to the main portion of
 * the program.
 */
public class LoginController extends Controller {
    /**
     * User name text entry field.
     */
    @FXML private TextField userNameField;
    /**
     * Password entry field.
     */
    @FXML private PasswordField passwordField;
    /**
     * Label showing the user's current timezone (ZoneID)
     */
    @FXML private Label locationText;
    /**
     * Button for submitting login information.
     */
    @FXML private Button loginButton;
    /**
     * Error message that will show when submitting invalid login credentials
     */
    @FXML private Label invalidLogin;
    /**
     * Error message that will show when there is a database error
     */
    @FXML private Label dbError;

    /**
     * Sets the location text once this view is created.
     * 
     * The lambda used here takes several fewer lines and is much more clear and straightforward
     * than the alternative of an anonymous overridden class as the parameter to the {@code
     * addListener} method.  The intention is "when the focused property changes, perform this
     * action."  The mechanism behind this is laid bare with the use of an overridden class (when
     * the property's value becomes invalid -- that is, it has changed and must be reevaluated --
     * the {@code invalidated()} method is called), and that only clutters the code and muddies
     * the simple intent of performing an action when the button looses focus.
     */
    @FXML
    public void initialize()
    {
        locationText.setText(ZoneId.systemDefault().toString());
        loginButton.focusedProperty().addListener(changed -> clearErrorMessages());
    }

    /**
     * Validates login credentials.  If successful, switches to Customers view.
     */
    @FXML
    private void validateLogin(ActionEvent event) {
        clearErrorMessages();
        String userName = userNameField.getText();
        String password = passwordField.getText();

        User authorizedUser;
        try {
            authorizedUser = new UserValidator().validate(userName, password);
        } catch (SQLException e) {
            System.err.println("Database error while validating user: " + e.getMessage());
            dbError.setVisible(true);
            return;
        }

        boolean successful = authorizedUser != null;
        try {
            logAttempt(userName, successful);
        } catch (IOException e) {
            System.err.println("Error logging login attempt.");
        }

        if (successful) {
            ViewLoader.setAuthorizedUser(authorizedUser);
            showUpcomingAppointments(authorizedUser);
            var loginWindow = ((Node) event.getSource()).getScene().getWindow();
            loginWindow.hide();
            try {
                ViewLoader.show("MainScreen");
            } catch (IOException e) {
                System.err.println("Error loading views: " + e.getMessage());
                System.exit(1);
            }
        } else {
            invalidLogin.setVisible(true);
        }
    }

    /**
     * Hides all error messages.
     */
    @FXML
    private void clearErrorMessages() {
        dbError.setVisible(false);
        invalidLogin.setVisible(false);
    }

    /**
     * Logs a login attempt to {@code login_activity.txt}.  Each login attempt is recorded as a
     * new line in the file with the user name, a timestamp, and an indicator of whether or not the
     * attempt was successful.
     *
     * @param user the name of the user attempting to log in
     * @param successful true if the login attempt was successful, false if not
     * @throws IOException if there is an error accessing the log file
     */
    private void logAttempt(String user, boolean successful) throws IOException {
        var logfileName = "login_activity.txt";
        var logfile = Path.of(logfileName);
        String logText = String.join("\t", user, Instant.now().toString(), successful ?
                "SUCCESS" : "FAIL");
        try (var logWriter = Files.newBufferedWriter(logfile, StandardOpenOption.CREATE,
                StandardOpenOption.APPEND)) {
            logWriter.write(logText);
            logWriter.newLine();
        }
    }

    /**
     * Opens an alert dialog that show appointments for {@code authorizedUser} that will start in
     * the next 15 minutes.  If there are no such appointments, shows a message stating so instead.
     *
     * @param authorizedUser the user who has just logged in
     */
    private void showUpcomingAppointments(User authorizedUser) {
        var alert = new Alert(Alert.AlertType.INFORMATION);
        // allow the dialog to auto-size itself to fit all the text:
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.setHeaderText(ViewLoader.getResource("Login.upcomingAppointments"));
        var alertText = new StringBuilder();
        try {
            var upcomingAppointments =
                    AppointmentDao.withUser(authorizedUser).getUpcomingAppointments(15);
            if (upcomingAppointments.isEmpty()) {
                alertText.append(ViewLoader.getResource("Login.upcomingAppointments.none"));
            } else {
                alert.setAlertType(Alert.AlertType.WARNING);
                for (Appointment appointment : upcomingAppointments) {
                    alertText.append(appointment.getId());
                    alertText.append(": ");
                    var start = appointment.getStart().atZone(ZoneId.systemDefault());
                    alertText.append(start.format(DateTimeFormatter
                            .ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT)));
                    alertText.append("\n\n");
                }
            }
        } catch (SQLException e) {
            alert.setAlertType(Alert.AlertType.ERROR);
            alertText.append(ViewLoader.getResource("Error.database.appointments"));
        }
        alert.setContentText(alertText.toString());
        alert.showAndWait();
    }
}