package scheduler.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Tab;

/**
 * Controls the main view of the Scheduler.
 */
public class MainScreenController extends Controller {
    /**
     * Customers view tab.
     */
    @FXML private Tab customersTab;
    /**
     * Appointments view tab.
     */
    @FXML private Tab appointmentsTab;
    /**
     * Reports view tab.
     */
    @FXML private Tab reportsTab;
    /**
     * Controller for the Customers view.
     */
    @FXML private CustomersController customersController;
    /**
     * Controller for the Appointments view.
     */
    @FXML private AppointmentsController appointmentsController;
    /**
     * Controller for the Reports view.
     */
    @FXML private ReportsController reportsController;

    /**
     * Updates the customers or appointments view when either tab is selected.
     */
    public void selectionChanged() {
        if (customersTab.isSelected()) customersController.update();
        else if (appointmentsTab.isSelected()) appointmentsController.update();
        else if (reportsTab.isSelected()) reportsController.update();
    }
}
