package scheduler.controller;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import scheduler.dao.AppointmentDao;
import scheduler.dao.ContactDao;
import scheduler.dao.CustomerDao;
import scheduler.model.Appointment;
import scheduler.model.Contact;

import java.sql.SQLException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Controls the Reports view.
 */
public class ReportsController extends Controller {
    /**
     * Indicates data is being loaded.
     */
    @FXML private ProgressIndicator progressIndicator;
    /**
     * Container for the different reports.
     */
    @FXML private Accordion accordion;
    /**
     * Container for {@code reportsAppointmentsSummaryText}.
     */
    @FXML private TitledPane reportsAppointmentsSummary;
    /**
     * Container for {@code reportsContactSchedulesText}.
     */
    @FXML private TitledPane reportsContactSchedules;
    /**
     * Container for {@code reportsCustomerLocationsText}.
     */
    @FXML private TitledPane reportsCustomerLocations;
    /**
     * Displays statistics for the appointments: number of appointments by month, and number of
     * appointments by type.
     */
    @FXML private TextArea reportsAppointmentsSummaryText;
    /**
     * Displays appointment schedules for each contact.
     */
    @FXML private TextArea reportsContactSchedulesText;
    /**
     * Displays statistics for customers: number of customers per division, and country totals.
     */
    @FXML private TextArea reportsCustomerLocationsText;
    /**
     * Error message for problems loading appointments data.
     */
    @FXML private Label reportsErrorAppointments;
    /**
     * Error message for problems loading contacts data.
     */
    @FXML private Label reportsErrorContacts;
    /**
     * Error message for problems loading contacts data.
     */
    @FXML private Label reportsErrorCustomers;

    /**
     * Sets a listener to update the reports when one is opened.
     * 
     * The lambda used here takes several fewer lines than the alternative of an anonymous
     * overridden class as the parameter to the {@code addListener} method while being at least
     * as clear
     */
    @FXML
    public void initialize() {
        accordion.expandedPaneProperty().addListener((observable, oldTitledPane, newTitledPane) -> update());
    }

    /**
     * Loads and displays the reports data for the currently expanded report.
     */
    public void update() {
        clearMessages();
        progressIndicator.setVisible(true);
        var thread = new Thread(new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    TitledPane expandedPane = accordion.getExpandedPane();
                    if (reportsAppointmentsSummary.equals(expandedPane)) {
                        updateAppointmentsSummary();
                    } else if (reportsContactSchedules.equals(expandedPane)) {
                        updateContactSchedules();
                    } else if (reportsCustomerLocations.equals(expandedPane)) {
                        updateCustomerLocations();
                    }
                } finally {
                    progressIndicator.setVisible(false);
                }
                return null;
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * Hides all messages.
     */
    private void clearMessages() {
        reportsErrorAppointments.setVisible(false);
    }

    /**
     * Loads and displays the appointments summary report.
     *
     * Both lambdas in this method serve similar purposes.  They each loop through a series of
     * paired values and add them to the display string (with some other formatting text).  This
     * could have been achieved with the use of a for-each loop, but the lambdas hide a bit of
     * the implementation details that do not actually improve clarity, saving a few lines and
     * showing the intent more clearly.
     */
    private void updateAppointmentsSummary() {
        var appointmentsSummary = new StringBuilder();
        appointmentsSummary.append(ViewLoader.getResource("Reports.appointmentsByMonth"));
        appointmentsSummary.append("\n");
        var appointmentDao = AppointmentDao.withUser(ViewLoader.getAuthorizedUser());
        try {
            appointmentDao.getStatsByMonth().forEach((month, number) -> {
                appointmentsSummary.append("\t");
                appointmentsSummary.append(month);
                appointmentsSummary.append(": ");
                appointmentsSummary.append(number);
                appointmentsSummary.append("\n");
            });
        } catch (SQLException e) {
            System.err.println("Database error while loading appointment data: " + e.getMessage());
            reportsErrorAppointments.setVisible(true);
            appointmentsSummary.append(ViewLoader.getResource("Error.database.loadData"));
            appointmentsSummary.append("\n");
        }
        appointmentsSummary.append("\n");
        appointmentsSummary.append(ViewLoader.getResource("Reports.appointmentsByType"));
        appointmentsSummary.append("\n");
        try {
            appointmentDao.getStatsByType().forEach((type, number) -> {
                appointmentsSummary.append("\t");
                appointmentsSummary.append(type);
                appointmentsSummary.append(": ");
                appointmentsSummary.append(number);
                appointmentsSummary.append("\n");
            });
        } catch (SQLException e) {
            System.err.println("Database error while loading appointment data: " + e.getMessage());
            reportsErrorAppointments.setVisible(true);
            appointmentsSummary.append(ViewLoader.getResource("Error.database.loadData"));
        }
        reportsAppointmentsSummaryText.setText(appointmentsSummary.toString());
    }

    /**
     * Loads and displays the contact schedules report.
     *
     * The lambda in this method cycles through a list o paired values and appends them (with
     * formatting) to the display string.  Alternative methods include a for-each loop which is a
     * bit less clear, and unrolling the loop and appending the values individually which takes
     * many more lines of code.
     */
    private void updateContactSchedules() {
        var contactSchedules = new StringBuilder();
        List<Contact> contacts;
        try {
            contacts = ContactDao.withUser(ViewLoader.getAuthorizedUser()).getAll();
        } catch (SQLException e) {
            System.err.println("Database error while loading contact data: " + e.getMessage());
            reportsErrorContacts.setVisible(true);
            reportsContactSchedulesText.setText(ViewLoader.getResource("Error.database.loadData"));
            return;
        }
        for (Contact contact : contacts) {
            contactSchedules.append(contact.getName());
            contactSchedules.append("\n\n");
            try {
                var appointments =
                        AppointmentDao.withUser(ViewLoader.getAuthorizedUser()).getAllForContact(contact);
                var dateTimeFormat = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG,
                        FormatStyle.SHORT);
                var localZone = ZoneId.systemDefault();
                final String RESOURCE_TAG = "resourceTag";
                final String VALUE = "value";
                for (Appointment appointment : appointments) {
                    Arrays.asList(
                            Map.of(RESOURCE_TAG, "verboseId", VALUE, appointment.getId()),
                            Map.of(RESOURCE_TAG, "title", VALUE, appointment.getTitle()),
                            Map.of(RESOURCE_TAG, "type", VALUE, appointment.getType()),
                            Map.of(RESOURCE_TAG, "description", VALUE,
                                    appointment.getDescription()),
                            Map.of(RESOURCE_TAG, "startTime", VALUE,
                                    appointment.getStart().atZone(localZone).format(dateTimeFormat)),
                            Map.of(RESOURCE_TAG, "endTime", VALUE,
                                    appointment.getEnd().atZone(localZone).format(dateTimeFormat)),
                            Map.of(RESOURCE_TAG, "customerId", VALUE,
                                    appointment.getCustomer().getId())
                    ).forEach(item -> {
                        contactSchedules.append("\t");
                        var resourceKey = "Appointment.column." + item.get(RESOURCE_TAG);
                        contactSchedules.append(ViewLoader.getResource(resourceKey));
                        contactSchedules.append(": ");
                        contactSchedules.append(item.get(VALUE));
                        contactSchedules.append("\n");
                    });
                    contactSchedules.append("\n");
                }
            } catch (SQLException e) {
                System.err.println("Database error while loading appointment data: " + e.getMessage());
                reportsErrorAppointments.setVisible(true);
                contactSchedules.append(ViewLoader.getResource("Error.database.loadData"));
                contactSchedules.append("\n\n");
            }
        }
        reportsContactSchedulesText.setText(contactSchedules.toString());
    }

    /**
     * Loads and displays the customer locations report.
     *
     * The lambda in this method takes fewer lines of code and is a bit less cluttered than a
     * for-each loop approach.  The intent is clearer.  Note that the for-each loop within the
     * lambda is a bit clearer than a lambda because it must access a variable from outside the
     * loop.  With a lambda, the variable would need to be an atomic which adds more complexity
     * than is needed here.
     */
    private void updateCustomerLocations() {
        var customerLocations = new StringBuilder();
        try {
            var stats = CustomerDao.withUser(ViewLoader.getAuthorizedUser()).getStatsByLocation();
            stats.forEach((countryName, divisionStats) -> {
                var numInCountry = 0;
                customerLocations.append(countryName).append("\n");
                for (var entry : divisionStats.entrySet()) {
                    String divisionName = entry.getKey();
                    Integer numInDivision = entry.getValue();
                    numInCountry += numInDivision;
                    customerLocations.append("\t");
                    customerLocations.append(divisionName);
                    customerLocations.append(": ");
                    customerLocations.append(numInDivision);
                    customerLocations.append("\n");
                }
                customerLocations.append(ViewLoader.getResource("Reports.total"));
                customerLocations.append(": ");
                customerLocations.append(numInCountry);
                customerLocations.append("\n\n");
            });
        } catch (SQLException e) {
            System.err.println("Database error while loading customer data: " + e.getMessage());
            reportsErrorCustomers.setVisible(true);
            customerLocations.append(ViewLoader.getResource("Error.database.loadData"));
        }
        reportsCustomerLocationsText.setText(customerLocations.toString());
    }
}
