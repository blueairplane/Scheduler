package scheduler.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import scheduler.dao.AppointmentDao;
import scheduler.dao.ContactDao;
import scheduler.dao.CustomerDao;
import scheduler.dao.UserDao;
import scheduler.model.Appointment;
import scheduler.model.Contact;

import java.sql.SQLException;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;

import static scheduler.Utilities.*;
import static scheduler.controller.ViewLoader.getAuthorizedUser;

/**
 * Controls the 'Update Appointment' form and its enclosing window.
 */
public class UpdateAppointmentController extends Controller {
    /**
     * Root node for this view.
     */
    @FXML public VBox updateAppointmentRoot;
    /**
     * Text entry for the appointment's title.
     */
    @FXML private TextField updateAppointmentTitle;
    /**
     * Disabled text entry for the appointment's ID number.
     */
    @FXML private TextField updateAppointmentId;
    /**
     * Text entry for the appointment's description.
     */
    @FXML private TextField updateAppointmentDescription;
    /**
     * Text entry for the appointment's type.
     */
    @FXML private TextField updateAppointmentType;
    /**
     * Text entry for the appointment's location.
     */
    @FXML private TextField updateAppointmentLocation;
    /**
     * Date selector for the appointment's start date.
     */
    @FXML private DatePicker updateAppointmentStartDate;
    /**
     * Pull-down selector of times for the appointment's start time.
     */
    @FXML private ComboBox<LocalTime> updateAppointmentStartTime;
    /**
     * Date selector for the appointment's end date.
     */
    @FXML private DatePicker updateAppointmentEndDate;
    /**
     * Pull-down selector of times for the appointment's end time.
     */
    @FXML private ComboBox<LocalTime> updateAppointmentEndTime;
    /**
     * Pull-down selector of contacts.
     */
    @FXML private ComboBox<Contact> updateAppointmentContact;
    /**
     * Text entry for the ID number of the customer associated with the appointment.
     */
    @FXML private TextField updateAppointmentCustomerId;
    /**
     * Text entry for the ID number of the user associated with the appointment.
     */
    @FXML private TextField updateAppointmentUserId;
    /**
     * Button for submitting appointment information.
     */
    @FXML private Button updateAppointmentButton;
    /**
     * Error message for problems with updating the appointment.
     */
    @FXML private Label updateAppointmentErrorUpdate;
    /**
     * Error message for database errors.
     */
    @FXML private Label updateAppointmentErrorDb;
    /**
     * Error message for invalid customer ID.
     */
    @FXML private Label updateAppointmentErrorInvalidCustomerId;
    /**
     * Error message for invalid user ID.
     */
    @FXML private Label updateAppointmentErrorInvalidUserId;
    /**
     * Error message for invalid appointment times.
     */
    @FXML private Label updateAppointmentErrorInvalidTimes;
    /**
     * Error message for overlapping appointment times.
     */
    @FXML private Label updateAppointmentErrorOverlap;

    /**
     * Sets up the form.  Installs formatters that enforce the length restrictions of the text
     * columns from the database.
     *
     * The lambda used here takes several fewer lines and is much more clear and straightforward
     * than the alternative of an anonymous overridden class as the parameter to the {@code
     * addListener} method.  The intention is "when the focused property changes, perform this
     * action."  The mechanism behind this is laid bare with the use of an overridden class (when
     * the property's value becomes invalid -- that is, it has changed and must be reevaluated --
     * the {@code invalidated()} method is called), and that only clutters the code and muddies
     * the simple intent of performing an action when the button looses focus.
     *
     * @throws SQLException if there is a database error
     */
    @FXML
    public void initialize() throws SQLException {
        var contactList = ContactDao.withUser(getAuthorizedUser()).getAll();
        var contacts = FXCollections.observableArrayList(contactList);
        updateAppointmentContact.setItems(contacts);
        var timeList = new ArrayList<LocalTime>();
        for (int hour = 0; hour < 24; hour++) {
            timeList.add(LocalTime.of(hour, 0));
        }
        var times = FXCollections.observableArrayList(timeList);
        updateAppointmentStartTime.setItems(times);
        updateAppointmentEndTime.setItems(times);
        updateAppointmentTitle.setTextFormatter(maxLengthFormatter(50));
        updateAppointmentDescription.setTextFormatter(maxLengthFormatter(50));
        updateAppointmentType.setTextFormatter(maxLengthFormatter(50));
        updateAppointmentLocation.setTextFormatter(maxLengthFormatter(50));
        updateAppointmentCustomerId.setTextFormatter(numberFormatter(10));
        updateAppointmentUserId.setTextFormatter(numberFormatter(10));
        updateAppointmentButton.focusedProperty().addListener(changed -> clearErrorMessages());
    }

    /**
     * Populates the form's fields with the given {@code Appointment}'s information.
     *
     * @param data the data.  It should be an {@code Appointment}.
     */
    public void setData(Object data) {
        var appointment = (Appointment) data;
        updateAppointmentTitle.setText(appointment.getTitle());
        updateAppointmentId.setText(appointment.getId().toString());
        updateAppointmentDescription.setText(appointment.getDescription());
        updateAppointmentType.setText(appointment.getType());
        updateAppointmentLocation.setText(appointment.getLocation());
        updateAppointmentContact.setValue(appointment.getContact());
        updateAppointmentCustomerId.setText(appointment.getCustomer().getId().toString());
        updateAppointmentUserId.setText(String.valueOf(appointment.getUser().getId()));
        var start = appointment.getStart().atZone(ZoneId.systemDefault());
        updateAppointmentStartDate.setValue(start.toLocalDate());
        updateAppointmentStartTime.setValue(start.toLocalTime());
        var end = appointment.getEnd().atZone(ZoneId.systemDefault());
        updateAppointmentEndDate.setValue(end.toLocalDate());
        updateAppointmentEndTime.setValue(end.toLocalTime());
    }

    /**
     * Attempts to update an appointment in the database with the information entered into the
     * form.  If any fields are empty or there is a database error, it will display an error
     * message and abort the update operation.  If the appointment is successfully updated, this
     * window will close.
     */
    @FXML
    private void updateAppointment() throws SQLException {
        clearErrorMessages();
        var title = updateAppointmentTitle.getText();
        var id = Integer.valueOf(updateAppointmentId.getText());
        var description = updateAppointmentDescription.getText();
        var location = updateAppointmentLocation.getText();
        var type = updateAppointmentType.getText();
        var startDate = updateAppointmentStartDate.getValue();
        var startTime = updateAppointmentStartTime.getValue();
        var endDate = updateAppointmentEndDate.getValue();
        var endTime = updateAppointmentEndTime.getValue();
        var customerId = updateAppointmentCustomerId.getText();
        var userId = updateAppointmentUserId.getText();
        var contact = updateAppointmentContact.getValue();
        if (title.equals("") || description.equals("") || location.equals("") || type.equals("")
                || startDate == null || startTime == null || endDate == null || endTime == null
                || customerId.equals("") || userId.equals("") || contact == null) {
            System.err.println("Could not store appointment: not all fields filled");
            updateAppointmentErrorUpdate.setVisible(true);
            return;
        }

        var start = ZonedDateTime.of(startDate, startTime, ZoneId.systemDefault()).toInstant();
        var end = ZonedDateTime.of(endDate, endTime, ZoneId.systemDefault()).toInstant();
        if (!validTimes(start, end)) {
            System.err.println("Could not store appointment: invalid times");
            updateAppointmentErrorInvalidTimes.setVisible(true);
            return;
        }

        var customer = CustomerDao.withUser(getAuthorizedUser()).get(Integer.parseInt(customerId));
        if (customer == null) {
            System.err.println("Invalid customer ID");
            updateAppointmentErrorInvalidCustomerId.setVisible(true);
            return;
        }
        var user = UserDao.withUser(getAuthorizedUser()).get(Integer.parseInt(userId));
        if (user == null) {
            System.err.println("Invalid user ID");
            updateAppointmentErrorInvalidUserId.setVisible(true);
            return;
        }

        var customerAppointments =
                AppointmentDao.withUser(getAuthorizedUser()).getAllForCustomer(customer);
        // don't check for conflicts with the appointment we are updating!
        customerAppointments.remove(AppointmentDao.withUser(getAuthorizedUser()).get(id));
        var newAppointment = new Appointment(id, title, description, location, type, start, end,
                customer, user, contact);
        if (newAppointment.conflictsWithAny(customerAppointments)) {
            System.err.println("Could not store appointment: overlaps with another of " +
                    customer.getName() + "'s appointments");
            updateAppointmentErrorOverlap.setVisible(true);
            return;
        }

        try {
            AppointmentDao.withUser(getAuthorizedUser()).update(newAppointment);
        } catch (SQLException e) {
            System.err.println("Database error: " + e.getMessage());
            updateAppointmentErrorDb.setVisible(true);
            return;
        }
        closeStage();
    }

    /**
     * Clears the entry fields and closes this window,
     */
    @FXML
    private void cancel() {
        closeStage();
    }

    /**
     * Clean up and closes this window.
     */
    private void closeStage() {
        updateAppointmentTitle.requestFocus();
        updateAppointmentTitle.clear();
        updateAppointmentId.clear();
        updateAppointmentDescription.clear();
        updateAppointmentType.clear();
        updateAppointmentLocation.clear();
        updateAppointmentContact.setItems(null);
        updateAppointmentCustomerId.clear();
        updateAppointmentUserId.clear();
        updateAppointmentStartDate.setValue(null);
        updateAppointmentEndDate.setValue(null);
        updateAppointmentStartTime.setValue(null);
        updateAppointmentEndTime.setValue(null);
        clearErrorMessages();
        updateAppointmentRoot.getScene().getWindow().hide();
    }

    /**
     * Hides all error messages.
     */
    private void clearErrorMessages() {
        updateAppointmentErrorUpdate.setVisible(false);
        updateAppointmentErrorDb.setVisible(false);
        updateAppointmentErrorInvalidTimes.setVisible(false);
        updateAppointmentErrorOverlap.setVisible(false);
        updateAppointmentErrorInvalidCustomerId.setVisible(false);
        updateAppointmentErrorInvalidUserId.setVisible(false);
    }
}
