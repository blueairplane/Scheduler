package scheduler.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import scheduler.dao.CountryDao;
import scheduler.dao.CustomerDao;
import scheduler.dao.DivisionDao;
import scheduler.model.Country;
import scheduler.model.Customer;
import scheduler.model.Division;

import java.sql.SQLException;
import java.util.List;

import static scheduler.Utilities.maxLengthFormatter;
import static scheduler.controller.ViewLoader.getAuthorizedUser;

/**
 * Controls the 'Update Customer' form and its enclosing window.
 */
public class UpdateCustomerController extends Controller {
    /**
     * Root node for this view.
     */
    @FXML public VBox updateCustomerRoot;
    /**
     * Text field for the customer's ID.  Not editable.
     */
    @FXML private TextField updateCustomerId;
    /**
     * Text entry for the customer's new full name.
     */
    @FXML private TextField updateCustomerName;
    /**
     * Text entry for the customer's new phone number.  It is not checked for formatting.
     */
    @FXML private TextField updateCustomerPhone;
    /**
     * Text entry for the customer's new address, up to the first-level-division.  Examples:
     * <ul>
     *     <li>U.S. address: 123 ABC Street, White Plains</li>
     *     <li>Canadian address: 123 ABC Street, Newmarket</li>
     *     <li>UK address: 123 ABC Street, Greenwich, London</li>
     * </ul>
     */
    @FXML private TextField updateCustomerAddress;
    /**
     * Text entry for the customer's new postal code.  It is not check for validity.
     */
    @FXML private TextField updateCustomerPostalCode;
    /**
     * Pull-down selector of first-level divisions.
     */
    @FXML private ComboBox<Division> updateCustomerDivision;
    /**
     * Pull-down selector of countries.
     */
    @FXML private ComboBox<Country> updateCustomerCountry;
    /**
     * Button for submitting customer information.
     */
    @FXML private Button updateCustomerButton;
    /**
     * Error message for problems with updating the customer's record.
     */
    @FXML private Label updateCustomerErrorUpdate;
    /**
     * Error message for database errors.
     */
    @FXML private Label updateCustomerErrorDb;

    /**
     * Sets up the form.  Installs formatters that enforce the length restrictions of the text
     * columns from the database.
     *
     * The lambda used here takes several fewer lines and is much more clear and straightforward
     * than the alternative of an anonymous overridden class as the parameter to the {@code
     * addListener} method.  The intention is "when the focused property changes, perform this
     * action."  The mechanism behind this is laid bare with the use of an overridden class (when
     * the property's value becomes invalid -- that is, it has changed and must be reevaluated --
     * the {@code invalidated()} method is called), and that only clutters the code and muddies
     * the simple intent of performing an action when the button looses focus.
     *
     * @throws SQLException if there is a database error
     */
    @FXML
    public void initialize() throws SQLException {
        var countryList = CountryDao.withUser(getAuthorizedUser()).getAll();
        var countries = FXCollections.observableArrayList(countryList);
        updateCustomerCountry.setItems(countries);
        updateCustomerName.setTextFormatter(maxLengthFormatter(50));
        updateCustomerPhone.setTextFormatter(maxLengthFormatter(50));
        updateCustomerAddress.setTextFormatter(maxLengthFormatter(100));
        updateCustomerPostalCode.setTextFormatter(maxLengthFormatter(50));
        updateCustomerButton.focusedProperty().addListener(changed -> clearErrorMessages());
    }

    /**
     * Populates the form's fields with data from the given {@code Customer}.
     *
     * @param data the data.  It should be a {@code Customer}.
     */
    public void setData(Object data) {
        Customer customer = (Customer) data;
        updateCustomerId.setText(customer.getId().toString());
        updateCustomerName.setText(customer.getName());
        updateCustomerPhone.setText(customer.getPhone());
        updateCustomerAddress.setText(customer.getAddress());
        updateCustomerPostalCode.setText(customer.getPostalCode());
        updateCustomerCountry.setValue(customer.getCountry());
        countryChanged();
        updateCustomerDivision.setValue(customer.getDivision());
    }

    /**
     * Attempts to update the customer in the database with the information entered into the form.
     * If any fields are empty or there is a database error, it will display an error message and
     * abort the update operation.  If the customer is successfully updated, this window will close.
     */
    @FXML
    private void updateCustomer() {
        var id = Integer.valueOf(updateCustomerId.getText());
        var name = updateCustomerName.getText();
        var address = updateCustomerAddress.getText();
        var postalCode = updateCustomerPostalCode.getText();
        var division = updateCustomerDivision.getValue();
        var phone = updateCustomerPhone.getText();
        if (name.equals("") || address.equals("") || postalCode.equals("") || phone.equals("") || division == null) {
            System.err.println("Could not update customer: not all fields filled");
            clearErrorMessages();
            updateCustomerErrorUpdate.setVisible(true);
            return;
        }
        var updatedCustomer = new Customer(id, name, address, postalCode, division, phone);
        try {
            CustomerDao.withUser(getAuthorizedUser()).update(updatedCustomer);
        } catch (SQLException e) {
            System.err.println("Database error: " + e.getMessage());
            clearErrorMessages();
            updateCustomerErrorDb.setVisible(true);
            return;
        }
        closeStage();
    }

    /**
     * Clears the entry fields and closes this window,
     */
    @FXML
    private void cancel() {
        closeStage();
    }

    /**
     * Clean up and closes this window.
     */
    private void closeStage() {
        updateCustomerName.requestFocus();
        updateCustomerName.clear();
        updateCustomerPhone.clear();
        updateCustomerAddress.clear();
        updateCustomerPostalCode.clear();
        updateCustomerCountry.setValue(null);
        updateCustomerDivision.setItems(null);
        updateCustomerDivision.setDisable(true);
        clearErrorMessages();
        updateCustomerRoot.getScene().getWindow().hide();
    }

    /**
     * Hides all error messages.
     */
    private void clearErrorMessages() {
        updateCustomerErrorUpdate.setVisible(false);
        updateCustomerErrorDb.setVisible(false);
    }

    /**
     * Attempts to retrieve the country's list of divisions and populate and activate the
     * division combo box when the user selects a country from the country combo box.
     */
    @FXML
    private void countryChanged() {
        var country = updateCustomerCountry.getValue();
        if (country == null) {
            return;
        }
        var divisionDao = DivisionDao.withUser(getAuthorizedUser());
        List<Division> divisionList;
        try {
            divisionList = divisionDao.getAllFromCountry(country);
        } catch (SQLException e) {
            System.err.println("Database error: " + e.getMessage());
            clearErrorMessages();
            updateCustomerErrorDb.setVisible(true);
            updateCustomerCountry.setValue(null);
            updateCustomerDivision.setItems(null);
            updateCustomerDivision.setDisable(true);
            return;
        }
        var divisions = FXCollections.observableArrayList(divisionList);
        updateCustomerDivision.setItems(divisions);
        updateCustomerDivision.setDisable(false);
        updateCustomerDivision.setValue(null);
    }
}
