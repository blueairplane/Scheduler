package scheduler.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import scheduler.model.User;

import java.io.IOException;
import java.util.ResourceBundle;

/**
 * Set of static functions that load and display FXML views.
 */
public class ViewLoader {
    /**
     * The user authorized to access the database the controllers use.
     */
    private static User authorizedUser;
    /**
     * The application's internationalization resources.
     */
    private static ResourceBundle resources;
    /**
     * The text that goes in the title bar for the application's window(s).
     */
    private static String windowTitle;

    /**
     * Returns the requested internationalization resource.
     *
     * @param resourceKey the key that refers to the requested resource
     * @return the string for the given key
     */
    public static String getResource(String resourceKey) {
        return resources.getString(resourceKey);
    }

    /**
     * Sets the internationalization resource bundle for this application.
     *
     * @param resources the internationalization resource bundle
     */
    public static void setResources(ResourceBundle resources) {
        ViewLoader.resources = resources;
    }

    /**
     * Returns the root node of the indicated view.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @return the root node of the desired view
     * @throws IOException if there is a problem loading the FXML file
     */
    private static Parent load(String name) throws IOException {
        return load(name, null);
    }

    /**
     * Returns the root node of the indicated view and passes data to its controller.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @param data data to be passed to the controller for the requested view
     * @return the root node of the desired view
     * @throws IOException if there is a problem loading the FXML file
     */
    private static Parent load(String name, Object data) throws IOException {
        var url = ViewLoader.class.getResource("/scheduler/view/" + name + ".fxml");
        var fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(url);
        fxmlLoader.setResources(ViewLoader.resources);
        Parent root = fxmlLoader.load();
        ((Controller) fxmlLoader.getController()).setData(data);
        return root;
    }

    /**
     * Displays the requested view.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @throws IOException if there is a problem loading the FXML file
     */
    public static void show(String name) throws IOException {
        show(name, null);
    }

    /**
     * Displays the requested view and passes data to its controller.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @param data data to be passed to the controller for the requested view
     * @throws IOException if there is a problem loading the FXML file
     */
    public static void show(String name, Object data) throws IOException {
        show(name, null, data);
    }

    /**
     * Displays the requested view using the given stage.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @param stage the stage on which to display the requested view
     * @throws IOException if there is a problem loading the FXML file
     */
    public static void show(String name, Stage stage) throws IOException {
        show(name, stage, null);
    }

    /**
     * Displays the requested view using the given stage and passes data to its controller.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @param stage the stage on which to display the requested view
     * @param data data to be passed to the controller for the requested view
     * @throws IOException if there is a problem loading the FXML file
     */
    public static void show(String name, Stage stage, Object data) throws IOException {
        Parent root = load(name, data);
        if (stage == null) stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setTitle(windowTitle);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Displays the requested view and blocks until it is closed.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @throws IOException if there is a problem loading the FXML file
     */
    public static void showAndWait(String name) throws IOException {
        showAndWait(name, null);
    }

    /**
     * Displays the requested view, passes data to its controller, and blocks until it is closed.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @param data data to be passed to the controller for the requested view
     * @throws IOException if there is a problem loading the FXML file
     */
    public static void showAndWait(String name, Object data) throws IOException {
        showAndWait(name, null, data);
    }

    /**
     * Displays the requested view using the given stage and blocks until it is closed.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @param stage the stage on which to display the requested view
     * @throws IOException if there is a problem loading the FXML file
     */
    public static void showAndWait(String name, Stage stage) throws IOException {
        showAndWait(name, stage, null);
    }

    /**
     * Displays the requested view using the given stage, passes data to its controller, and
     * blocks until it is closed.
     *
     * @param name the name of the FXML file (without the extension) for the desired view
     * @param stage the stage on which to display the requested view
     * @param data data to be passed to the controller for the requested view
     * @throws IOException if there is a problem loading the FXML file
     */
    public static void showAndWait(String name, Stage stage, Object data) throws IOException {
        Parent root = load(name, data);
        if (stage == null) stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setTitle(windowTitle);
        stage.setResizable(false);
        stage.showAndWait();
    }

    /**
     * Returns the user authorized to access the database.
     *
     * @return the authorized user
     */
    public static User getAuthorizedUser() {
        return authorizedUser;
    }

    /**
     * Sets the user authorized to access the database.
     *
     * @param authorizedUser the authorized user
     */
    public static void setAuthorizedUser(User authorizedUser) {
        ViewLoader.authorizedUser = authorizedUser;
    }

    /**
     * Sets the text for the title bar of windows for this application.
     *
     * @param windowTitle the title bar text
     */
    public static void setWindowTitle(String windowTitle) {
        ViewLoader.windowTitle = windowTitle;
    }
}
