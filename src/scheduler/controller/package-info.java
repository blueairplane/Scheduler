/**
 * Contains classes that control the views of the Scheduler application.
 */
package scheduler.controller;