package scheduler.dao;

import scheduler.model.Appointment;
import scheduler.model.Contact;
import scheduler.model.Customer;
import scheduler.model.User;

import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static scheduler.Utilities.instantToSqlDateTime;
import static scheduler.Utilities.sqlDateTimeToInstant;

/**
 * AppointmentDao allows CRUD access to the appointments table of the database.
 */
public class AppointmentDao extends AuthorizedDao implements DaoRead<Appointment>,
        DaoCreateUpdateDelete<Appointment> {
    /**
     * Map of {@code AppointmentDao} instances for given authorized users.
     */
    private static final Map<User, AppointmentDao> daoMap = new HashMap<>();

    /**
     * Returns the {@code AppointmentDao} for the given authorized user.
     *
     * @param authorizedUser the user authorized to access the database
     * @return the data access abject associated with the give authorized user
     */
    public static AppointmentDao withUser(User authorizedUser) {
        if (!daoMap.containsKey(authorizedUser)) {
            daoMap.put(authorizedUser, new AppointmentDao(authorizedUser));
        }
        return daoMap.get(authorizedUser);
    }

    /**
     * Creates a data access object for the given authorized user to access appointment data.
     *
     * @param authorizedUser the user authorized to access the database
     * @throws IllegalArgumentException if {@code authorizedUser} is null
     */
    private AppointmentDao(User authorizedUser) throws IllegalArgumentException {
        super(authorizedUser);
    }

    /**
     * Retrieves appointment data from the database and creates a new {@code Appointment} object.
     * Returns null if no appointment data can be found with the given ID number.
     *
     * Note: technically, this retrieves the first result for the given ID number.  The ID number
     * should be the primary key of the database, so this should not be a problem, but if the
     * database has errors and does not enforce the uniqueness of the {@code Appointment_ID} column,
     * this could produce unexpected results.
     *
     * @param id the ID number (primary key) of the appointment
     * @return the new {@code Appointment} object based on the retrieved data, or null if no such
     * data exists
     * @throws SQLException if there is a database error
     */
    @Override
    public Appointment get(int id) throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  app.Appointment_ID id, " +
                    "  app.Title title, " +
                    "  app.Description description, " +
                    "  app.Location location, " +
                    "  app.Type type, " +
                    "  app.Start start, " +
                    "  app.End end, " +
                    "  cus.Customer_ID customer_id, " +
                    "  usr.User_ID user_id, " +
                    "  con.Contact_ID contact_id " +
                    "FROM appointments app " +
                    "JOIN customers cus " +
                    "  ON app.Customer_ID = cus.Customer_ID " +
                    "JOIN users usr " +
                    "  ON app.User_ID = usr.User_ID " +
                    "JOIN contacts con " +
                    "  ON app.Contact_ID = con.Contact_ID " +
                    "WHERE Appointment_ID = ?";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setInt(1, id);
                try (var results = ps.executeQuery()) {
                    if (results.next()) {
                        var resultId = results.getInt("id");
                        var resultTitle = results.getString("title");
                        var resultDescription = results.getString("description");
                        var resultLocation = results.getString("location");
                        var resultType = results.getString("type");
                        var resultStart = sqlDateTimeToInstant(results.getString("start"));
                        var resultEnd = sqlDateTimeToInstant(results.getString("end"));
                        var resultUserId = results.getInt("user_id");
                        var resultCustomerId = results.getInt("customer_id");
                        var resultContactId = results.getInt("contact_id");
                        var customer = CustomerDao.withUser(authorizedUser).get(resultCustomerId);
                        var user = UserDao.withUser(authorizedUser).get(resultUserId);
                        var contact = ContactDao.withUser(authorizedUser).get(resultContactId);
                        return new Appointment(resultId, resultTitle, resultDescription,
                                resultLocation, resultType, resultStart, resultEnd, customer,
                                user, contact);
                    }
                    else {
                        return null;
                    }
                }
            }
        } catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.get: " + e.getMessage());
        }
    }

    /**
     * Retrieves a list of {@code Appointment} objects based on the appointment data in the
     * database.  If the database has no entries, this returns an empty list.
     *
     * The lambdas here are just as clear and straightforward as a for-each loop while saving a
     * little bit of vertical space.
     *
     * @return the list of appointments
     * @throws SQLException if there is a database error
     */
    @Override
    public List<Appointment> getAll() throws SQLException {
        // preload data from other tables so they're not queried multiple times:
        var customers = new HashMap<Integer, Customer>();
        CustomerDao.withUser(authorizedUser).getAll().forEach(customer ->
                customers.put(customer.getId(), customer));
        var users = new HashMap<Integer, User>();
        UserDao.withUser(authorizedUser).getAll().forEach(user ->
                users.put(user.getId(), user));
        var contacts = new HashMap<Integer, Contact>();
        ContactDao.withUser(authorizedUser).getAll().forEach(contact ->
                contacts.put(contact.getId(), contact));

        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  app.Appointment_ID id, " +
                    "  app.Title title, " +
                    "  app.Description description, " +
                    "  app.Location location, " +
                    "  app.Type type, " +
                    "  app.Start start, " +
                    "  app.End end, " +
                    "  cus.Customer_ID customer_id, " +
                    "  usr.User_ID user_id, " +
                    "  con.Contact_ID contact_id " +
                    "FROM appointments app " +
                    "JOIN customers cus " +
                    "  ON app.Customer_ID = cus.Customer_ID " +
                    "JOIN users usr " +
                    "  ON app.User_ID = usr.User_ID " +
                    "JOIN contacts con " +
                    "  ON app.Contact_ID = con.Contact_ID " +
                    "ORDER BY app.Start, app.End, app.Appointment_ID";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                List<Appointment> appointments = new ArrayList<>();
                while (results.next()) {
                    var resultId = results.getInt("id");
                    var resultTitle = results.getString("title");
                    var resultDescription = results.getString("description");
                    var resultLocation = results.getString("location");
                    var resultType = results.getString("type");
                    var resultStart = sqlDateTimeToInstant(results.getString("start"));
                    var resultEnd = sqlDateTimeToInstant(results.getString("end"));
                    var resultUserId = results.getInt("user_id");
                    var resultCustomerId = results.getInt("customer_id");
                    var resultContactId = results.getInt("contact_id");
                    var customer = customers.getOrDefault(resultCustomerId, null);
                    var user = users.getOrDefault(resultUserId, null);
                    var contact = contacts.getOrDefault(resultContactId, null);
                    var appointment = new Appointment(resultId, resultTitle, resultDescription,
                            resultLocation, resultType, resultStart, resultEnd, customer, user,
                            contact);
                    appointments.add(appointment);
                }
                return appointments;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.getAll: " + e.getMessage());
        }
    }

    /**
     * Retrieves a list of appointments held by {@code contact} based on the appointment data in
     * the database.  If the contact has no appointments, this returns an empty list.
     *
     * The lambdas here are just as clear and straightforward as a for-each loop while saving a
     * little bit of vertical space.
     *
     * @param contact the contact whose appointments are being retrieved
     * @return the list of appointments for {@code contact}
     * @throws SQLException if there is a database error
     */
    public List<Appointment> getAllForContact(Contact contact) throws SQLException {
        // preload data from other tables so they're not queried multiple times:
        var users = new HashMap<Integer, User>();
        UserDao.withUser(authorizedUser).getAll().forEach(user ->
                users.put(user.getId(), user));
        var customers = new HashMap<Integer, Customer>();
        CustomerDao.withUser(authorizedUser).getAll().forEach(customer ->
                customers.put(customer.getId(), customer));
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  app.Appointment_ID id, " +
                    "  app.Title title, " +
                    "  app.Description description, " +
                    "  app.Location location, " +
                    "  app.Type type, " +
                    "  app.Start start, " +
                    "  app.End end, " +
                    "  usr.User_ID user_id, " +
                    "  cus.Customer_ID customer_id " +
                    "FROM appointments app " +
                    "JOIN customers cus " +
                    "  ON app.Customer_ID = cus.Customer_ID " +
                    "JOIN users usr " +
                    "  ON app.User_ID = usr.User_ID " +
                    "JOIN contacts con " +
                    "  ON app.Contact_ID = con.Contact_ID " +
                    "WHERE app.Contact_ID = ? " +
                    "ORDER BY app.Start, app.End, app.Appointment_ID";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setInt(1, contact.getId());
                try (var results = ps.executeQuery()) {
                    List<Appointment> appointments = new ArrayList<>();
                    while (results.next()) {
                        var resultId = results.getInt("id");
                        var resultTitle = results.getString("title");
                        var resultDescription = results.getString("description");
                        var resultLocation = results.getString("location");
                        var resultType = results.getString("type");
                        var resultStart = sqlDateTimeToInstant(results.getString("start"));
                        var resultEnd = sqlDateTimeToInstant(results.getString("end"));
                        var resultUserId = results.getInt("user_id");
                        var resultCustomerId = results.getInt("customer_id");
                        var user = users.getOrDefault(resultUserId, null);
                        var customer = customers.getOrDefault(resultCustomerId, null);
                        var appointment = new Appointment(resultId, resultTitle, resultDescription,
                                resultLocation, resultType, resultStart, resultEnd, customer, user,
                                contact);
                        appointments.add(appointment);
                    }
                    return appointments;
                }
            }
        } catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.getAllForContact: " + e.getMessage());
        }
    }

    /**
     * Retrieves a list of appointments held by {@code customer} based on the appointment data in
     * the database.  If the customer has no appointments, this returns an empty list.
     *
     * The lambdas here are just as clear and straightforward as a for-each loop while saving a
     * little bit of vertical space.
     *
     * @param customer the customer whose appointments are being retrieved
     * @return the list of appointments for {@code customer}
     * @throws SQLException if there is a database error
     */
    public List<Appointment> getAllForCustomer(Customer customer) throws SQLException {
        // preload data from other tables so they're not queried multiple times:
        var users = new HashMap<Integer, User>();
        UserDao.withUser(authorizedUser).getAll().forEach(user ->
                users.put(user.getId(), user));
        var contacts = new HashMap<Integer, Contact>();
        ContactDao.withUser(authorizedUser).getAll().forEach(contact ->
                contacts.put(contact.getId(), contact));

        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  app.Appointment_ID id, " +
                    "  app.Title title, " +
                    "  app.Description description, " +
                    "  app.Location location, " +
                    "  app.Type type, " +
                    "  app.Start start, " +
                    "  app.End end, " +
                    "  usr.User_ID user_id, " +
                    "  con.Contact_ID contact_id " +
                    "FROM appointments app " +
                    "JOIN customers cus " +
                    "  ON app.Customer_ID = cus.Customer_ID " +
                    "JOIN users usr " +
                    "  ON app.User_ID = usr.User_ID " +
                    "JOIN contacts con " +
                    "  ON app.Contact_ID = con.Contact_ID " +
                    "WHERE app.Customer_ID = ? " +
                    "ORDER BY app.Start, app.End, app.Appointment_ID";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setInt(1, customer.getId());
                try (var results = ps.executeQuery()) {
                    List<Appointment> appointments = new ArrayList<>();
                    while (results.next()) {
                        var resultId = results.getInt("id");
                        var resultTitle = results.getString("title");
                        var resultDescription = results.getString("description");
                        var resultLocation = results.getString("location");
                        var resultType = results.getString("type");
                        var resultStart = sqlDateTimeToInstant(results.getString("start"));
                        var resultEnd = sqlDateTimeToInstant(results.getString("end"));
                        var resultUserId = results.getInt("user_id");
                        var resultContactId = results.getInt("contact_id");
                        var user = users.getOrDefault(resultUserId, null);
                        var contact = contacts.getOrDefault(resultContactId, null);
                        var appointment = new Appointment(resultId, resultTitle, resultDescription,
                                resultLocation, resultType, resultStart, resultEnd, customer, user,
                                contact);
                        appointments.add(appointment);
                    }
                    return appointments;
                }
            }
        } catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.getAllForCustomer: " + e.getMessage());
        }
    }

    /**
     * Retrieves the number of appointments by month as a mapping sorted by month.
     *
     * @return the map of values
     * @throws SQLException if there is a database error
     */
    public SortedMap<String, Integer> getStatsByMonth() throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  DATE_FORMAT(Start, '%Y-%m') yearmonth, " +
                    "  count(*) number " +
                    "FROM appointments " +
                    "GROUP BY yearmonth " +
                    "ORDER BY yearmonth";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                    SortedMap<String, Integer> stats = new TreeMap<>();
                    while (results.next()) {
                        var month = results.getString("yearmonth");
                        var number = results.getInt("number");
                        stats.put(month, number);
                    }
                    return stats;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.getStatsByMonth: " + e.getMessage());
        }
    }

    /**
     * Retrieves the number of appointments by type as a mapping sorted alphabetically by type.
     *
     * @return the map of values
     * @throws SQLException if there is a database error
     */
    public SortedMap<String, Integer> getStatsByType() throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  Type type, " +
                    "  count(*) number " +
                    "FROM appointments " +
                    "GROUP BY type " +
                    "ORDER BY type";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                SortedMap<String, Integer> stats = new TreeMap<>();
                while (results.next()) {
                    var month = results.getString("type");
                    var number = results.getInt("number");
                    stats.put(month, number);
                }
                return stats;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.getStatsByType: " + e.getMessage());
        }
    }

    /**
     * Returns a list of appointments for {@code authorizedUser} that will start within {@code
     * minutesFromNow} minutes.  If there are no such appointments, returns an empty list.
     *
     * The lambdas here are just as clear and straightforward as a for-each loop while saving a
     * little bit of vertical space.
     *
     * @param minutesFromNow how many minutes in the future the appointment can start
     * @return a list of appointments (empty if there are none that match
     * @throws SQLException if there is a database error
     */
    public List<Appointment> getUpcomingAppointments(int minutesFromNow) throws SQLException {
        // preload data from other tables so they're not queried multiple times:
        var customers = new HashMap<Integer, Customer>();
        CustomerDao.withUser(authorizedUser).getAll().forEach(customer ->
                customers.put(customer.getId(), customer));
        var users = new HashMap<Integer, User>();
        UserDao.withUser(authorizedUser).getAll().forEach(user ->
                users.put(user.getId(), user));
        var contacts = new HashMap<Integer, Contact>();
        ContactDao.withUser(authorizedUser).getAll().forEach(contact ->
                contacts.put(contact.getId(), contact));

        var now = Instant.now();
        var then = now.plus(minutesFromNow, ChronoUnit.MINUTES);
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  app.Appointment_ID id, " +
                    "  app.Title title, " +
                    "  app.Description description, " +
                    "  app.Location location, " +
                    "  app.Type type, " +
                    "  app.Start start, " +
                    "  app.End end, " +
                    "  cus.Customer_ID customer_id, " +
                    "  usr.User_ID user_id, " +
                    "  con.Contact_ID contact_id " +
                    "FROM appointments app " +
                    "JOIN customers cus " +
                    "  ON app.Customer_ID = cus.Customer_ID " +
                    "JOIN users usr " +
                    "  ON app.User_ID = usr.User_ID " +
                    "JOIN contacts con " +
                    "  ON app.Contact_ID = con.Contact_ID " +
                    "WHERE " +
                    "  app.Start >= ? " +
                    "  AND app.Start <= ?";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setString(1, instantToSqlDateTime(now));
                ps.setString(2, instantToSqlDateTime(then));
                try (var results = ps.executeQuery()) {
                    List<Appointment> appointments = new ArrayList<>();
                    while (results.next()) {
                        var resultId = results.getInt("id");
                        var resultTitle = results.getString("title");
                        var resultDescription = results.getString("description");
                        var resultLocation = results.getString("location");
                        var resultType = results.getString("type");
                        var resultStart = sqlDateTimeToInstant(results.getString("start"));
                        var resultEnd = sqlDateTimeToInstant(results.getString("end"));
                        var resultUserId = results.getInt("user_id");
                        var resultCustomerId = results.getInt("customer_id");
                        var resultContactId = results.getInt("contact_id");
                        var customer = customers.getOrDefault(resultCustomerId, null);
                        var user = users.getOrDefault(resultUserId, null);
                        var contact = contacts.getOrDefault(resultContactId, null);
                        var appointment = new Appointment(resultId, resultTitle, resultDescription,
                                resultLocation, resultType, resultStart, resultEnd, customer, user,
                                contact);
                        appointments.add(appointment);
                    }
                    return appointments;
                }
            }
        } catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.getUpcomingAppointments: " + e.getMessage());
        }
    }

    /**
     * Creates a new row in the table with the data from the given appointment.  As part of this, it
     * timestamps the new entry.  This will always attempt to add the appointment to the database,
     * ignoring the appointment's ID which will be automatically generated by the database.
     *
     * @param appointment the appointment to be added to the database
     * @throws SQLException if there is a database error
     */
    @Override
    public void add(Appointment appointment) throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            var sql = "" +
                    "INSERT INTO " +
                    "appointments (" +
                    "  Title, " +
                    "  Description, " +
                    "  Location, " +
                    "  Type, " +
                    "  Start, " +
                    "  End, " +
                    "  Customer_ID, " +
                    "  User_ID, " +
                    "  Contact_ID, " +
                    "  Created_By, " +
                    "  Last_Updated_By, " +
                    "  Create_Date, " +
                    "  Last_Update " +
                    ") VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setString(1, appointment.getTitle());
                ps.setString(2, appointment.getDescription());
                ps.setString(3, appointment.getLocation());
                ps.setString(4, appointment.getType());
                ps.setString(5, instantToSqlDateTime(appointment.getStart()));
                ps.setString(6, instantToSqlDateTime(appointment.getEnd()));
                ps.setInt(7, appointment.getCustomer().getId());
                ps.setInt(8, appointment.getUser().getId());
                ps.setInt(9, appointment.getContact().getId());
                ps.setString(10, authorizedUser.getName());
                ps.setString(11, authorizedUser.getName());
                var now = instantToSqlDateTime(Instant.now());
                ps.setString(12, now);
                ps.setString(13, now);
                ps.executeUpdate();
            }
        }
        catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.add: " + e.getMessage());
        }
    }

    /**
     * Replaces the data in the row with the same ID number as {@code appointment} with the data
     * from the given appointment.  As part of this, it timestamps the update.
     *
     * @param appointment the appointment to be updated in the database
     * @return true on success, false if there is no appointment with a matching ID in the database
     * @throws SQLException if there is a database error
     */
    @Override
    public boolean update(Appointment appointment) throws SQLException {
        if (appointment.getId() == null) {
            return false;
        }
        try (var conn = databaseConnector.getConnection()) {
            var sql = "" +
                    "UPDATE appointments " +
                    "  SET Title = ?, " +
                    "      Description = ?, " +
                    "      Location = ?, " +
                    "      Type = ?, " +
                    "      Start = ?, " +
                    "      End = ?, " +
                    "      Customer_ID = ?, " +
                    "      User_ID = ?, " +
                    "      Contact_ID = ?, " +
                    "      Last_Update = ?, " +
                    "      Last_Updated_By = ? " +
                    "WHERE Appointment_ID = ?";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setString(1, appointment.getTitle());
                ps.setString(2, appointment.getDescription());
                ps.setString(3, appointment.getLocation());
                ps.setString(4, appointment.getType());
                ps.setString(5, instantToSqlDateTime(appointment.getStart()));
                ps.setString(6, instantToSqlDateTime(appointment.getEnd()));
                ps.setInt(7, appointment.getCustomer().getId());
                ps.setInt(8, appointment.getUser().getId());
                ps.setInt(9, appointment.getContact().getId());
                ps.setString(10, instantToSqlDateTime(Instant.now()));
                ps.setString(11, authorizedUser.getName());
                ps.setInt(12, appointment.getId());
                var affectedRowsNum = ps.executeUpdate();
                return affectedRowsNum != 0;
            }
        }
        catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.update: " + e.getMessage());
        }
    }

    /**
     * Removes the row corresponding to the given appointment from the database.
     *
     * @param appointment the appointment to be removed from the database
     * @return true if successful, false if the appointment is not in the database
     * @throws SQLException if there is a database error
     */
    @Override
    public boolean delete(Appointment appointment) throws SQLException {
        if (appointment.getId() == null) {
            return false;
        }
        try (var conn = databaseConnector.getConnection()) {
            var sql = "" +
                    "DELETE " +
                    "FROM appointments " +
                    "WHERE Appointment_ID = ?";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setInt(1, appointment.getId());
                int affectedRowsNum = ps.executeUpdate();
                return affectedRowsNum != 0;
            }
        }
        catch (SQLException e) {
            throw new SQLException("Error in AppointmentDao.delete: " + e.getMessage());
        }
    }
}
