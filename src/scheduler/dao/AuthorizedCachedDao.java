package scheduler.dao;

import scheduler.model.User;

import java.sql.SQLException;
import java.util.*;

/**
 * Base class for a data access object that is authorized to read from a read-only database table
 * and caches the data to reduce database queries.  It should not be used for very large tables
 * without overloading the {@code get()} method, as it always loads the entire dataset even for a
 * single use of the retrieval of one item.
 *
 * @param <T> the type of the dataset contained in the table
 */
public abstract class AuthorizedCachedDao<T extends UniqueId> extends AuthorizedDao implements DaoRead<T> {
    /**
     * A list of objects from the table.  If there is a specific order the data should be in, it
     * should be set in the list returned from {@code retrieveData()}.  This list is an exact
     * copy whose order will not change.
     */
    protected final List<T> dataList = new ArrayList<>();
    /**
     * A map of the objects from the table for quick retrieval by ID number.
     */
    protected final Map<Integer, T> dataMap = new HashMap<>();

    /**
     * Creates a data access object for the given authorized user to access the database.
     *
     * @param authorizedUser the user authorized to access the database
     * @throws IllegalArgumentException if {@code authorizedUser} is null
     */
    public AuthorizedCachedDao(User authorizedUser) throws IllegalArgumentException {
        super(authorizedUser);
    }

    /**
     * Returns a single object, by ID number, from the database.  This loads all the objects
     * accessed by this DAO if they have not yet been loaded.
     *
     * @param id the ID number of the data.  This should correspond to an integer primary key.
     * @return the requested object, or null if no such object exists
     * @throws SQLException if there is a database error
     */
    @Override
    public T get(int id) throws SQLException {
        if (dataList.isEmpty()) {
            try {
                populateData();
            } catch (SQLException e) {
                throw new SQLException("Error in " + getClass().getName() + ".get: " + e.getMessage());
            }
        }
        return dataMap.getOrDefault(id, null);
    }

    /**
     * Returns a list of all the objects in the dataset.
     *
     * @return a list of all the objects in the dataset.  If the dataset is empty, it should
     * return an empty list
     * @throws SQLException if there is a database error
     */
    @Override
    public List<T> getAll() throws SQLException {
        if (dataList.isEmpty()) {
            try {
                populateData();
            } catch (SQLException e) {
                throw new SQLException("Error in " + getClass().getName() + ".getAll: " + e.getMessage());
            }
        }
        return Collections.unmodifiableList(dataList);
    }

    /**
     * Populates both the list and map of data.
     *
     * @throws SQLException possibly thrown by {@code retrieveData()} when querying the database
     */
    protected void populateData() throws SQLException {
        dataList.addAll(retrieveData());
        for (T item : dataList) {
            Integer id = item.getId();
            dataMap.put(id, item);
        }
    }

    /**
     * Gets the full list of objects from the table this DAO accesses.  The returned list's order
     * will be preserved.
     *
     * @return the full list of objects
     * @throws SQLException if there is an error in querying the database
     */
    protected abstract List<T> retrieveData() throws SQLException;
}
