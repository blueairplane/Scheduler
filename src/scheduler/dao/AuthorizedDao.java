package scheduler.dao;

import scheduler.model.User;

/**
 * Base class for a data access object that has authorization to fully access the database.
 */
public abstract class AuthorizedDao extends Dao {
    /**
     * The user who will be accessing the database.
     */
    protected final User authorizedUser;

    /**
     * Creates a data access object for the given authorized user to access the database.
     *
     * @param authorizedUser the user authorized to access the database
     * @throws IllegalArgumentException if {@code authorizedUser} is null
     */
    public AuthorizedDao(User authorizedUser) throws IllegalArgumentException {
        if (authorizedUser == null) {
            throw new IllegalArgumentException("authorizedUser cannot be null");
        }
        this.authorizedUser = authorizedUser;
    }
}
