package scheduler.dao;

import scheduler.model.Contact;
import scheduler.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ContactDao allows read access to the Contact table of the database.  It is a read-only
 * database, so this is the only functionality possible for the contact data.
 */
public class ContactDao extends AuthorizedCachedDao<Contact> implements DaoRead<Contact> {
    /**
     * Map of {@code ContactDao} instances for given authorized users.
     */
    private static final Map<User, ContactDao> daoMap = new HashMap<>();

    /**
     * Returns the {@code ContactDao} for the given authorized user.
     *
     * @param authorizedUser the user authorized to access the database
     * @return the data access abject associated with the give authorized user
     */
    public static ContactDao withUser(User authorizedUser) {
        if (!daoMap.containsKey(authorizedUser)) {
            daoMap.put(authorizedUser, new ContactDao(authorizedUser));
        }
        return daoMap.get(authorizedUser);
    }

    /**
     * Creates a data access object for the given authorized user to access the database.
     *
     * @param authorizedUser the user authorized to access the database
     * @throws IllegalArgumentException if {@code authorizedUser} is null
     */
    private ContactDao(User authorizedUser) throws IllegalArgumentException {
        super(authorizedUser);
    }

    /**
     * Retrieves a list of {@code Contact} objects based on the contact data in the database.  If
     * the database has no entries, this returns an empty list.
     *
     * @return the list of contacts
     * @throws SQLException if there is a database error
     */
    @Override
    protected List<Contact> retrieveData() throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  Contact_ID id, " +
                    "  Contact_Name name, " +
                    "  Email email " +
                    "FROM contacts";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                List<Contact> contacts = new ArrayList<>();
                while (results.next()) {
                    var id = results.getInt("id");
                    var name = results.getString("name");
                    var email = results.getString("email");
                    var contact = new Contact(id, name, email);
                    contacts.add(contact);
                }
                return contacts;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in ContactDao.getAll: " + e.getMessage());
        }
    }
}
