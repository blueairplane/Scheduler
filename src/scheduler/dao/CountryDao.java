package scheduler.dao;

import scheduler.model.Country;
import scheduler.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CountryDao allows read access to the Country table of the database.  It is a read-only
 * database, so this is the only functionality possible for the country data.
 */
public class CountryDao extends AuthorizedCachedDao<Country> implements DaoRead<Country> {
    /**
     * Map of {@code CountryDao} instances for given authorized users.
     */
    private static final Map<User, CountryDao> daoMap = new HashMap<>();

    /**
     * Returns the {@code CountryDao} for the given authorized user.
     *
     * @param authorizedUser the user authorized to access the database
     * @return the data access abject associated with the give authorized user
     */
    public static CountryDao withUser(User authorizedUser) {
        if (!daoMap.containsKey(authorizedUser)) {
            daoMap.put(authorizedUser, new CountryDao(authorizedUser));
        }
        return daoMap.get(authorizedUser);
    }

    /**
     * Creates a data access object for the given authorized user to access the database.
     *
     * @param authorizedUser the user authorized to access the database
     * @throws IllegalArgumentException if {@code authorizedUser} is null
     */
    public CountryDao(User authorizedUser) throws IllegalArgumentException {
        super(authorizedUser);
    }

    /**
     * Retrieves a list of {@code Country} objects based on the country data in the database.  If
     * the database has no entries, this returns an empty list.
     *
     * @return the list of countries
     * @throws SQLException if there is a database error
     */
    @Override
    protected List<Country> retrieveData() throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  Country_ID id, " +
                    "  Country name " +
                    "FROM countries " +
                    "ORDER BY Country";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                List<Country> countries = new ArrayList<>();
                while (results.next()) {
                    var id = results.getInt("id");
                    var name = results.getString("name");
                    var country = new Country(id, name);
                    countries.add(country);
                }
                return countries;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in CountryDao.getAll: " + e.getMessage());
        }
    }
}
