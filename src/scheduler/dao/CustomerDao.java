package scheduler.dao;

import scheduler.model.Country;
import scheduler.model.Customer;
import scheduler.model.Division;
import scheduler.model.User;

import java.sql.SQLException;
import java.time.Instant;
import java.util.*;

import static scheduler.Utilities.instantToSqlDateTime;

/**
 * CustomerDao allows CRUD access to the customers table of the database.
 */
public class CustomerDao extends AuthorizedDao implements DaoRead<Customer>,
        DaoCreateUpdateDelete<Customer> {
    /**
     * Map of {@code CustomerDao} instances for given authorized users.
     */
    private static final Map<User, CustomerDao> daoMap = new HashMap<>();

    /**
     * Returns the {@code CustomerDao} for the given authorized user.
     *
     * @param authorizedUser the user authorized to access the database
     * @return the data access abject associated with the give authorized user
     */
    public static CustomerDao withUser(User authorizedUser) {
        if (!daoMap.containsKey(authorizedUser)) {
            daoMap.put(authorizedUser, new CustomerDao(authorizedUser));
        }
        return daoMap.get(authorizedUser);
    }

    /**
     * Creates a data access object for the given authorized user to access customer data.
     *
     * @param authorizedUser the user name of the authorized user
     * @throws IllegalArgumentException if {@code authorizedUser} is null
     */
    public CustomerDao(User authorizedUser) throws IllegalArgumentException {
        super(authorizedUser);
    }

    /**
     * Retrieves customer data from the database and creates a new {@code Customer} object. Returns
     * null if no customer data can be found with the given ID number.
     *
     * Note: technically, this retrieves the first result for the given ID number.  The ID number
     * should be the primary key of the database, so this should not be a problem, but if the
     * database has errors and does not enforce the uniqueness of the {@code Customer_ID} column,
     * this could produce unexpected results.
     *
     * @param id the ID number (primary key) of the customer
     * @return the new {@code Customer} object based on the retrieved data, or null if no such
     * data exists
     * @throws SQLException if there is a database error
     */
    @Override
    public Customer get(int id) throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  cus.Customer_ID id, " +
                    "  cus.Customer_Name name, " +
                    "  cus.Address address, " +
                    "  cus.Postal_Code postalCode, " +
                    "  cus.Phone phone, " +
                    "  fld.Division_ID division_id, " +
                    "  fld.Division division_name, " +
                    "  cty.Country_ID country_id, " +
                    "  cty.Country country_name " +
                    "FROM customers cus " +
                    "JOIN first_level_divisions fld " +
                    "  ON cus.Division_ID = fld.Division_ID " +
                    "JOIN countries cty " +
                    "  ON fld.Country_ID = cty.Country_ID " +
                    "WHERE Customer_ID = ?";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setInt(1, id);
                try (var results = ps.executeQuery()) {
                    if (results.next()) {
                        var resultId = results.getInt("id");
                        var resultName = results.getString("name");
                        var resultAddress = results.getString("address");
                        var resultPostalCode = results.getString("postalCode");
                        var resultPhone = results.getString("phone");
                        var resultDivisionId = results.getInt("division_id");
                        var resultDivisionName = results.getString("division_name");
                        var resultCountryId = results.getInt("country_id");
                        var resultCountryName = results.getString("country_name");
                        var country = new Country(resultCountryId, resultCountryName);
                        var division = new Division(resultDivisionId, resultDivisionName, country);
                        return new Customer(resultId, resultName, resultAddress, resultPostalCode,
                                division, resultPhone);
                    }
                    else {
                        return null;
                    }
                }
            }
        } catch (SQLException e) {
            throw new SQLException("Error in CustomerDao.get: " + e.getMessage());
        }
    }

    /**
     * Retrieves a list of {@code Customer} objects based on the customer data in the database.  If
     * the database has no entries, this returns an empty list.
     *
     * @return the list of customers
     * @throws SQLException if there is a database error
     */
    @Override
    public List<Customer> getAll() throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  cus.Customer_ID id, " +
                    "  cus.Customer_Name name, " +
                    "  cus.Address address, " +
                    "  cus.Postal_Code postalCode, " +
                    "  cus.Phone phone, " +
                    "  fld.Division_ID division_id, " +
                    "  fld.Division division_name, " +
                    "  cty.Country_ID country_id, " +
                    "  cty.Country country_name " +
                    "FROM customers cus " +
                    "JOIN first_level_divisions fld " +
                    "  ON cus.Division_ID = fld.Division_ID " +
                    "JOIN countries cty " +
                    "  ON fld.Country_ID = cty.Country_ID " +
                    "ORDER BY cus.Customer_ID";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                List<Customer> customers = new ArrayList<>();
                while (results.next()) {
                    var resultId = results.getInt("id");
                    var resultName = results.getString("name");
                    var resultAddress = results.getString("address");
                    var resultPostalCode = results.getString("postalCode");
                    var resultPhone = results.getString("phone");
                    var resultDivisionId = results.getInt("division_id");
                    var resultDivisionName = results.getString("division_name");
                    var resultCountryId = results.getInt("country_id");
                    var resultCountryName = results.getString("country_name");
                    var country = new Country(resultCountryId, resultCountryName);
                    var division = new Division(resultDivisionId, resultDivisionName, country);
                    var customer = new Customer(resultId, resultName, resultAddress,
                            resultPostalCode, division, resultPhone);
                    customers.add(customer);
                }
                return customers;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in CustomerDao.getAll: " + e.getMessage());
        }
    }

    /**
     * Retrieves statistics of the number of customers in each division and country.
     *
     * @return a sorted map of statistics.  The format is {country_name: {division_name:
     * number_in_division}}.
     * @throws SQLException if there is a database error
     */
    public SortedMap<String, SortedMap<String, Integer>> getStatsByLocation() throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  fld.Division division_name, " +
                    "  cty.Country country_name, " +
                    "  count(cus.Customer_ID) number " +
                    "FROM customers cus " +
                    "JOIN first_level_divisions fld " +
                    "  ON cus.Division_ID = fld.Division_ID " +
                    "JOIN countries cty " +
                    "  ON fld.Country_ID = cty.Country_ID " +
                    "GROUP BY " +
                    "  country_name, " +
                    "  division_name";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                SortedMap<String, SortedMap<String, Integer>> locationStats = new TreeMap<>();
                while (results.next()) {
                    var resultDivisionName = results.getString("division_name");
                    var resultCountryName = results.getString("country_name");
                    var resultNumber = results.getInt("number");
                    if (!locationStats.containsKey(resultCountryName)) {
                        locationStats.put(resultCountryName, new TreeMap<>());
                    }
                    locationStats.get(resultCountryName).put(resultDivisionName, resultNumber);
                }
                return locationStats;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in CustomerDao.getStatsByLocation: " + e.getMessage());
        }
    }

    /**
     * Creates a new row in the table with the data from the given customer.  As part of this, it
     * timestamps the new entry.  This will always attempt to add the customer to the database,
     * ignoring the customer's ID which will be automatically generated by the database.
     *
     * @param customer the customer to be added to the database
     * @throws SQLException if there is a database error
     */
    @Override
    public void add(Customer customer) throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            var sql = "" +
                    "INSERT INTO " +
                    "customers (" +
                    "  Customer_Name, " +
                    "  Address, " +
                    "  Postal_Code, " +
                    "  Phone, " +
                    "  Division_ID, " +
                    "  Created_By, " +
                    "  Last_Updated_By, " +
                    "  Create_Date, " +
                    "  Last_Update " +
                    ") VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setString(1, customer.getName());
                ps.setString(2, customer.getAddress());
                ps.setString(3, customer.getPostalCode());
                ps.setString(4, customer.getPhone());
                ps.setInt(5, customer.getDivision().getId());
                ps.setString(6, authorizedUser.getName());
                ps.setString(7, authorizedUser.getName());
                var now = instantToSqlDateTime(Instant.now());
                ps.setString(8, now);
                ps.setString(9, now);
                ps.executeUpdate();
            }
        }
        catch (SQLException e) {
            throw new SQLException("Error in CustomerDao.add: " + e.getMessage());
        }
    }

    /**
     * Replaces the data in the row with the same ID number as {@code customer} with the data from
     * the given customer.  As part of this, it timestamps the update.
     *
     * @param customer the customer to be updated in the database
     * @return true on success, false if there is no customer with a matching ID in the database
     * @throws SQLException if there is a database error
     */
    @Override
    public boolean update(Customer customer) throws SQLException {
        if (customer.getId() == null) {
            return false;
        }
        try (var conn = databaseConnector.getConnection()) {
            var sql = "" +
                    "UPDATE customers " +
                    "  SET Customer_Name = ?, " +
                    "      Address = ?, " +
                    "      Postal_Code = ?, " +
                    "      Phone = ?, " +
                    "      Division_ID = ?, " +
                    "      Last_Update = ?, " +
                    "      Last_Updated_By = ? " +
                    "WHERE Customer_ID = ?";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setString(1, customer.getName());
                ps.setString(2, customer.getAddress());
                ps.setString(3, customer.getPostalCode());
                ps.setString(4, customer.getPhone());
                ps.setInt(5, customer.getDivision().getId());
                ps.setString(6, instantToSqlDateTime(Instant.now()));
                ps.setString(7, authorizedUser.getName());
                ps.setInt(8, customer.getId());
                var affectedRowsNum = ps.executeUpdate();
                return affectedRowsNum != 0;
            }
        }
        catch (SQLException e) {
            throw new SQLException("Error in CustomerDao.update: " + e.getMessage());
        }
    }

    /**
     * Removes the row corresponding to the given customer from the database.
     *
     * @param customer the customer to be removed from the database
     * @return true if successful, false if the customer is not in the database
     * @throws SQLException if there is a database error
     */
    @Override
    public boolean delete(Customer customer) throws SQLException {
        if (customer.getId() == null) {
            return false;
        }
        try (var conn = databaseConnector.getConnection()) {
            var removeAppointments = "" +
                    "DELETE " +
                    "FROM appointments " +
                    "WHERE Customer_ID = ?";
            var removeCustomer = "" +
                    "DELETE " +
                    "FROM customers " +
                    "WHERE Customer_ID = ?";
            try (var psAppointments = conn.prepareStatement(removeAppointments);
                 var psCustomer = conn.prepareStatement(removeCustomer)) {
                psAppointments.setInt(1, customer.getId());
                psCustomer.setInt(1, customer.getId());
                psAppointments.executeUpdate();
                int affectedRowsNum = psCustomer.executeUpdate();
                return affectedRowsNum != 0;
            }
        }
        catch (SQLException e) {
            throw new SQLException("Error in CustomerDao.delete: " + e.getMessage());
        }
    }
}
