package scheduler.dao;

/**
 * Base class for all data access objects.
 */
public abstract class Dao {
    /**
     * Provides connections to this application's database.
     */
    protected final DatabaseConnector databaseConnector = new DatabaseConnector();
}
