package scheduler.dao;

import java.sql.SQLException;

/**
 * A data access object that can create, update, and delete rows from the database.
 *
 * @param <T> the type of the objects this DaoCreateUpdateDelete object accesses
 */
public interface DaoCreateUpdateDelete<T> {
    /**
     * Creates a new row in the table with the given object.
     *
     * @param t the object to be added to the database
     * @throws SQLException if there is a database error
     */
    void add(T t) throws SQLException;

    /**
     * Replaces the data in the row with the same primary key as {@code t} with the data from the
     * given object.
     *
     * @param t the object to be updated in the database
     * @return true on success, false if there is no row corresponding to the object's primary key
     * @throws SQLException if there is a database error
     */
    boolean update(T t) throws SQLException;

    /**
     * Removes the row corresponding to the given object from the database.
     *
     * @param t the object to be removed from the database
     * @return true if successful, false if there is no row corresponding to the object's primary
     * key in the database
     * @throws SQLException if there is a database error
     */
    boolean delete(T t) throws SQLException;
}
