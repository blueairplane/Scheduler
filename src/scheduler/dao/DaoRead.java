package scheduler.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * A data access object that can read from the database.
 *
 * @param <T> the type of the objects this DaoRead object accesses
 */
public interface DaoRead<T> {
    /**
     * Returns a single object, by ID number, from the database.
     *
     * @param id the ID number of the data.  This should correspond to an integer primary key.
     * @return the requested object, or null if no such object exists
     * @throws SQLException if there is a database error
     */
    T get(int id) throws SQLException;

    /**
     * Returns a list of all the objects in the dataset.
     *
     * @return a list of all the objects in the dataset.  If the dataset is empty, it should
     * return an empty list
     * @throws SQLException if there is a database error
     */
    List<T> getAll() throws SQLException;
}
