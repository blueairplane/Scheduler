package scheduler.dao;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Enables the establishment of a connection to the database for this application.  The data
 * source is shared by {@code DatabaseConnector} objects.  Database settings are pulled from the
 * db.properties file in the {@code scheduler} package.
 */
public class DatabaseConnector {
    /**
     * Shared DataSource for the database used by this application.
     */
    private static DataSource dataSource = null;

    /**
     * Creates an object that can connect to this application's database.  If the data source has
     * not been created yet (that is, this is the first {@code DatabaseConnector} to be created),
     * it will be created based on the setting found in the db.properties file in the {@code
     * scheduler} package.
     */
    public DatabaseConnector() {
        if (dataSource == null) {
            var mysqlDataSource = new MysqlDataSource();
            var dbProperties = new Properties();
            try {
                dbProperties.load(getClass().getResourceAsStream("/scheduler/db.properties"));
            }
            catch (IOException e) {
                System.err.println("Error loading database settings.");
                System.err.println(e.getMessage());
                System.exit(1);
            }
            mysqlDataSource.setServerName(dbProperties.getProperty("servername"));
            mysqlDataSource.setPort(Integer.parseInt(dbProperties.getProperty("port")));
            mysqlDataSource.setUser(dbProperties.getProperty("user"));
            mysqlDataSource.setPassword(dbProperties.getProperty("password"));
            mysqlDataSource.setDatabaseName(dbProperties.getProperty("dbname"));
            dataSource = mysqlDataSource;
        }
    }

    /**
     * Returns a connection to the database for this application.
     *
     * @return the database connection
     * @throws SQLException if there is a database connection error
     */
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
