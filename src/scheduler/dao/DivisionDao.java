package scheduler.dao;

import scheduler.model.Country;
import scheduler.model.Division;
import scheduler.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * DivisionDao allows read access to the Division table of the database.  It is a read-only
 * database, so this is the only functionality possible for the first-level division data.
 */
public class DivisionDao extends AuthorizedCachedDao<Division> implements DaoRead<Division> {
    /**
     * Map of {@code DivisionDao} instances for given authorized users.
     */
    private static final Map<User, DivisionDao> daoMap = new HashMap<>();

    /**
     * Returns the {@code DivisionDao} for the given authorized user.
     *
     * @param authorizedUser the user authorized to access the database
     * @return the data access abject associated with the give authorized user
     */
    public static DivisionDao withUser(User authorizedUser) {
        if (!daoMap.containsKey(authorizedUser)) {
            daoMap.put(authorizedUser, new DivisionDao(authorizedUser));
        }
        return daoMap.get(authorizedUser);
    }

    /**
     * Creates a data access object for the given authorized user to access the database.
     *
     * @param authorizedUser the user authorized to access the database
     * @throws IllegalArgumentException if {@code authorizedUser} is null
     */
    public DivisionDao(User authorizedUser) throws IllegalArgumentException {
        super(authorizedUser);
    }

    /**
     * Retrieves a list of {@code Division} objects from the indicated country based on the
     * first-level division data in the database.  If the database has no entries, this returns
     * an empty list.
     *
     * The lambda in this method (as part of a stream pipeline) is a concise, extremely clear
     * presentation of the intention of the code.  The alternative of a for-each loop takes more
     * lines of code and is no clearer.
     *
     * @param country the country with which to filter the divisions
     * @return the list of first-level divisions
     * @throws SQLException if there is a database error
     */
    public List<Division> getAllFromCountry(Country country) throws SQLException {
        try {
            return getAll()
                    .stream()
                    .filter(division -> division.getCountry().equals(country))
                    .collect(Collectors.toList());
        } catch (SQLException e) {
            throw new SQLException("Error in DivisionDao.getAllFromCountry: " + e.getMessage());
        }
    }

    /**
     * Retrieves a list of {@code Division} objects based on the first-level division data in the
     * database.  If the database has no entries, this returns an empty list.
     *
     * @return the list of first-level divisions
     * @throws SQLException if there is a database error
     */
    @Override
    protected List<Division> retrieveData() throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  d.Division_ID id, " +
                    "  d.Division name, " +
                    "  c.Country_ID country_id, " +
                    "  c.Country country_name " +
                    "FROM first_level_divisions d " +
                    "JOIN countries c " +
                    "  ON d.Country_ID = c.Country_ID " +
                    "ORDER BY d.Division";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                List<Division> divisions = new ArrayList<>();
                while (results.next()) {
                    var resultId = results.getInt("id");
                    var resultName = results.getString("name");
                    var resultCountryId = results.getInt("country_id");
                    var resultCountryName = results.getString("country_name");
                    var country = new Country(resultCountryId, resultCountryName);
                    var division = new Division(resultId, resultName, country);
                    divisions.add(division);
                }
                return divisions;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in DivisionDao.getAll: " + e.getMessage());
        }
    }
}
