package scheduler.dao;

/**
 * Describes an object that has a unique ID number.
 */
public interface UniqueId {
    /**
     * Returns the unique ID number for this object.
     *
     * @return the unique ID number
     */
    int getId();
}
