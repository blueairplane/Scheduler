package scheduler.dao;

import scheduler.model.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * UserDao allows read access to the 'users' table of the database.
 */
public class UserDao extends AuthorizedCachedDao<User> implements DaoRead<User> {
    /**
     * Map of {@code UserDao} instances for given authorized users.
     */
    private static final Map<User, UserDao> daoMap = new HashMap<>();

    /**
     * Returns the {@code UserDao} for the given authorized user.
     *
     * @param authorizedUser the user authorized to access the database
     * @return the data access abject associated with the give authorized user
     */
    public static UserDao withUser(User authorizedUser) {
        if (!daoMap.containsKey(authorizedUser)) {
            daoMap.put(authorizedUser, new UserDao(authorizedUser));
        }
        return daoMap.get(authorizedUser);
    }

    /**
     * Creates a data access object for the given authorized user to access the database.
     *
     * @param authorizedUser the user authorized to access the database
     * @throws IllegalArgumentException if {@code authorizedUser} is null
     */
    public UserDao(User authorizedUser) throws IllegalArgumentException {
        super(authorizedUser);
    }

    /**
     * Returns a list of all users in the database.  If there are no users in the database,
     * returns an empty list.
     *
     * @return a list of all the users in the database.  If the database has no users, returns an
     * empty list
     * @throws SQLException if there is a database error
     */
    @Override
    protected List<User> retrieveData() throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  User_ID id, " +
                    "  User_Name name " +
                    "FROM users";
            try (var ps = conn.prepareStatement(sql);
                 var results = ps.executeQuery()) {
                List<User> users = new ArrayList<>();
                while (results.next()) {
                    var id = results.getInt("id");
                    var name = results.getString("name");
                    var user = new User(id, name);
                    users.add(user);
                }
                return users;
            }
        } catch (SQLException e) {
            throw new SQLException("Error in UserDao.getAll: " + e.getMessage());
        }
    }
}
