package scheduler.dao;

import scheduler.model.User;

import java.sql.SQLException;

/**
 * Connects to the 'users' table of the database to validate logins.
 */
public class UserValidator extends Dao {
    /**
     * Validates a user name and password combination and returns a valid {@code User} object.
     *
     * @param userName the user name for this attempted login
     * @param password the password for this attempted login
     * @return the validated {@code User} object
     * @throws SQLException if there is a database error
     */
    public User validate(String userName, String password) throws SQLException {
        try (var conn = databaseConnector.getConnection()) {
            String sql = "" +
                    "SELECT " +
                    "  User_ID id " +
                    "FROM users " +
                    "WHERE BINARY User_Name = ? " +
                    "  AND BINARY Password = ?";
            try (var ps = conn.prepareStatement(sql)) {
                ps.setString(1, userName);
                ps.setString(2, password);
                try (var result = ps.executeQuery()) {
                    if (result.next()) {
                        return new User(result.getInt("id"), userName);
                    }
                    else {
                        return null;
                    }
                }
            }
        }
        catch (SQLException e) {
            throw new SQLException("Error in UserValidator.validate: " + e.getMessage());
        }
    }
}
