/**
 * Contains classes that connect to and retrieve and store information in the database.
 */
package scheduler.dao;