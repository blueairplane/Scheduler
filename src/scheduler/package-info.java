/**
 * Main package for the Scheduler application.  Scheduler allows the user to view, create, edit,
 * and delete customer and appointment data.  It also shows reports on the data.
 */
package scheduler;